import requests
import random
import sqlite3

DATABASE_FILE = "C:\\magshimim\\c++\\16\\trivia_project\\Trivia\\triviaDB.sqlite"
QUESTIONS_TO_INSERT = 1000

# Function to create the database connection
def create_connection():
    conn = sqlite3.connect(DATABASE_FILE)
    return conn

# Function to create the Question table if it doesn't exist
def create_table(conn):
    query = '''
    CREATE TABLE IF NOT EXISTS Question (
        ID INTEGER PRIMARY KEY AUTOINCREMENT,
        QUESTION_TEXT TEXT NOT NULL,
        ANSWER1 TEXT NOT NULL,
        ANSWER2 TEXT NOT NULL,
        ANSWER3 TEXT NOT NULL,
        ANSWER4 TEXT NOT NULL,
        CORRECT_ANSWER INTEGER NOT NULL
    );
    '''
    with conn:
        conn.execute(query)

# Function to insert a question into the database
def insert_question(conn, question):
    query = "INSERT INTO Question (QUESTION_TEXT, ANSWER1, ANSWER2, ANSWER3, ANSWER4, CORRECT_ANSWER) VALUES (?, ?, ?, ?, ?, ?)"
    with conn:
        conn.execute(query, (question['question'], *question['answers'], question['correct_answer']))

# Function to fetch questions from the OpenTDB API
def fetch_questions():
    response = requests.get('https://opentdb.com/api.php?amount=1')
    if response.status_code == 200:
        result = response.json()
        if result['response_code'] == 0 and len(result['results']) > 0:
            questions = []
            for question in result['results']:
                question_text = question['question']
                answers = question['incorrect_answers']
                answers.append(question['correct_answer'])
                random.shuffle(answers)
                correct_answer = answers.index(question['correct_answer']) + 1
                if len(answers) == 4:
                    questions.append({
                        'question': question_text,
                        'answers': answers,
                        'correct_answer': correct_answer
                    })
            return questions
    return None

# Main script
def main():
    # Create the database connection
    

    # Create the Question table if it doesn't exist
   

    # Fetch questions from the API
    while True:
        questions = fetch_questions()
        if questions:
            questions = questions[0]
            print("================================================\n" + questions['question'] + "\n================================================\n")
            i = 0
            for answer in questions['answers']:
                i += 1
                print(str(i) + ".   " + answer + "\n")
            num = input("Please enter the number of the right answer: ")
            if int(num) == int(questions['correct_answer']):
                print("""____    __    ____  __  .__   __. .__   __.  _______ .______       __  
\   \  /  \  /   / |  | |  \ |  | |  \ |  | |   ____||   _  \     |  | 
 \   \/    \/   /  |  | |   \|  | |   \|  | |  |__   |  |_)  |    |  | 
  \            /   |  | |  . `  | |  . `  | |   __|  |      /     |  | 
   \    /\    /    |  | |  |\   | |  |\   | |  |____ |  |\  \----.|__| 
    \__/  \__/     |__| |__| \__| |__| \__| |_______|| _| `._____|(__) 
                                                                       """)
            else:
                print(""".___________.    ___       __  ___  _______    .___________. __    __   _______     __      
|           |   /   \     |  |/  / |   ____|   |           ||  |  |  | |   ____|   |  |     
`---|  |----`  /  ^  \    |  '  /  |  |__      `---|  |----`|  |__|  | |  |__      |  |     
    |  |      /  /_\  \   |    <   |   __|         |  |     |   __   | |   __|     |  |     
    |  |     /  _____  \  |  .  \  |  |____        |  |     |  |  |  | |  |____    |  `----.
    |__|    /__/     \__\ |__|\__\ |_______|       |__|     |__|  |__| |_______|   |_______|
                                                                                            """)
                print("\n=============================\nThe correct answer is: \n" + questions['answers'][int(questions['correct_answer'])] + "\n=============================\n")

    # Close the database connection
   

# Run the script
if __name__ == '__main__':
    main()
