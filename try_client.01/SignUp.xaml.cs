﻿using System.Windows;
using System.Windows.Controls;

namespace try_client._01
{
    public partial class SignUp : Page
    {
        public SignUp()
        {
            InitializeComponent();
        }

        private void SignUp_Click(object sender, RoutedEventArgs e)
        {
            string username = usernameTextBox.Text;
            string password = passwordBox.Password;
            string email = EmailBox.Text;

            // Create the JSON object for signup
            var signupData = new
            {
                username = username,
                password = password,
                email = email
            };

            // Save the username in the App class
            ((App)Application.Current).Username = username;

            // Send the signup JSON object to the server
            ((App)Application.Current).SendMessageToServer(2, signupData, null!);
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            // Stop the program execution
            Application.Current.Shutdown();
        }
    }
}
