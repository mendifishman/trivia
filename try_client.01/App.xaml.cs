﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Newtonsoft.Json;

namespace try_client._01
{
    public partial class App : Application
    {
        private TcpClient? client = null;
        public NetworkStream? stream = null;
        private Choice? choiceWindow = null;
        public string? Username { get; set; }
        public object? RoomData { get; set; }
        public int MessageType { get; set; }
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            ConnectToServer();

            // Create and show the login window
            choiceWindow = new Choice();
            choiceWindow.Show();

            // Subscribe to the Exit event to ensure the socket is closed before the program exits
            Current.Exit += Current_Exit;
        }

        private void Current_Exit(object sender, ExitEventArgs e)
        {
            DisconnectFromServer();
        }

        private void ConnectToServer()
        {
            client = new TcpClient();
            client.Connect("127.0.0.1", 5030);
            stream = client.GetStream();
        }

        private void DisconnectFromServer()
        {
            // Close the stream and client if they are not null
            stream?.Close();
            client?.Close();
        }

        public void SendMessageToServer(int messageType, object data, dynamic roomData)
        {
            try
            {
                string json = JsonConvert.SerializeObject(data); // Convert the data to JSON
                byte[] jsonDataBytes = Encoding.UTF8.GetBytes(json);
                byte[] message = new byte[5 + jsonDataBytes.Length];

                // Set the message type as the first byte
                message[0] = (byte)messageType;

                // Set the length of the JSON data as the next four bytes
                byte[] lengthBytes = BitConverter.GetBytes(jsonDataBytes.Length);
                if (BitConverter.IsLittleEndian)
                    Array.Reverse(lengthBytes);
                Array.Copy(lengthBytes, 0, message, 1, 4);

                // Copy the JSON data bytes into the message
                Array.Copy(jsonDataBytes, 0, message, 5, jsonDataBytes.Length);

                // Send the message to the server if the stream is not null
                if (stream != null)
                {
                    stream.Write(message, 0, message.Length);
                    stream.Flush();
                }
                else
                {
                    MessageBox.Show("Error: Connection to the server is not established.");
                }

                // Wait for the server response here
                byte[] responseHeader = new byte[5];
                if (stream != null)
                {
                    stream.Read(responseHeader, 0, responseHeader.Length);
                }
                byte responseType = responseHeader[0];
                int jsonSize = BitConverter.ToInt32(responseHeader, 1);
                byte[] jsonResponse = new byte[jsonSize];
                if (stream != null)
                {
                    stream.Read(jsonResponse, 0, jsonResponse.Length);
                }
                MessageType = responseType;
                if (responseType == 20)
                {
                    string serverJson = Encoding.UTF8.GetString(jsonResponse);
                    if (messageType == 3)
                    {
                        var playersData = new
                        {
                            players = new List<string> { Username }
                        };

                        // Convert the playersData to JSON
                        string playersJson = JsonConvert.SerializeObject(playersData);
                        Room roomWin = new Room(playersJson, roomData);
                        roomWin.Show();
                    }
                    else if (messageType == 7)
                    {
                        PersonalStats statsWindow = new PersonalStats(serverJson);
                        statsWindow.Show();
                    }
                    else if (messageType == 4)
                    {
                        WatchRooms watchRooms = new WatchRooms(serverJson);
                        watchRooms.Show();
                    }
                    else if (messageType == 6 || messageType == 12)
                    {
                        if (messageType == 6)
                        {
                            SendMessageToServer(12, data, roomData);
                        }
                        else
                        {
                            Room roomWin = new Room(serverJson, roomData);
                            roomWin.Show();
                            messageType = 0;
                        }
                    }
                    else if (messageType == 8)
                    {
                        HighScores statsWindow = new HighScores(serverJson);
                        statsWindow.Show();
                    }
                    else if(messageType==17)
                    {
                        GameResults gameResults = new GameResults(serverJson);
                        gameResults.Show();
                    }
                    else if(messageType==14 ||messageType==13 || messageType==9 || messageType==11)
                    {

                    }
                    else
                    {
                        Game mainWindow = new Game();
                        mainWindow.Show();
                        choiceWindow?.Close();
                    }
                }
                else
                {
                    MessageBox.Show("Error: Server response was not successful.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error sending message to server: " + ex.Message);
            }
        }
    }
}