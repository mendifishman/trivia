﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace try_client._01
{
    public partial class Choice : Window
    {
        public Choice()
        {
            InitializeComponent();
        }

        private void SignUp_Click(object sender, RoutedEventArgs e)
        {
            SignUp signup = new SignUp();
            contentFrame.Navigate(signup);

            // Hide the SignUp button
            SignUpButton.Visibility = Visibility.Collapsed;
            LoginButton.Visibility = Visibility.Collapsed;
        }

        private void Login_Click(object sender, RoutedEventArgs e)
        {
            LogInWindow loginWindow = new LogInWindow();
            contentFrame.Navigate(loginWindow);

            // Hide the Login button
            SignUpButton.Visibility = Visibility.Collapsed;
            LoginButton.Visibility = Visibility.Collapsed;
        }
    }
}