﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Timers;

namespace try_client._01
{
    /// <summary>
    /// Interaction logic for TriviaGame.xaml
    /// </summary>
    public partial class TriviaGame : Window
    {
        private Timer timer;
        private bool isButtonPressed;
        private int time;
        private int NumOfQuestions;
        private int counter = 1;
        private string playersJson;
        private object roomData;
        public TriviaGame(int timePerQuestion, int numOfQuestions, string PlayersJson, object RoomData)
        {
            time = timePerQuestion;
            playersJson = PlayersJson;
            roomData = RoomData;
            InitializeComponent();
            Loaded += TriviaGame_Loaded;
            NumOfQuestions = numOfQuestions;
        }

        private void TriviaGame_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                SendMessageToServer();
                StartTimer();
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Error occurred: {ex.Message}");
            }
        }

        private void StartTimer()
        {
            timer = new Timer(time * 1000); // Set the interval to 10 seconds (10,000 milliseconds)
            timer.Elapsed += Timer_Elapsed;
            timer.Start();
        }

        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (!isButtonPressed)
            {
                // Simulate a button press on the first button
                Application.Current.Dispatcher.Invoke(() =>
                {
                    if (answersItemsControl.Items.Count > 0)
                    {
                        Button firstButton = answersItemsControl.Items[0] as Button;
                        AnswerButton_Click(firstButton, null);
                    }
                });
            }
            else
            {
                isButtonPressed = false;
            }
        }




        private void SendMessageToServer()
        {
            int serverTypeMsg = 20;
            if (Application.Current is App app && app.stream != null)
            {
                string json = JsonConvert.SerializeObject(null); // Convert the data to JSON
                byte[] jsonDataBytes = Encoding.UTF8.GetBytes(json);
                byte[] message = new byte[5 + jsonDataBytes.Length];

                // Set the message type as the first byte
                message[0] = (byte)15;

                // Set the length of the JSON data as the next four bytes
                byte[] lengthBytes = BitConverter.GetBytes(jsonDataBytes.Length);
                if (BitConverter.IsLittleEndian)
                    Array.Reverse(lengthBytes);
                Array.Copy(lengthBytes, 0, message, 1, 4);

                // Copy the JSON data bytes into the message
                Array.Copy(jsonDataBytes, 0, message, 5, jsonDataBytes.Length);

                // Send the message to the server if the stream is not null
                if (app.stream != null)
                {
                    app.stream.Write(message, 0, message.Length);
                    app.stream.Flush();
                }
                else
                {
                    MessageBox.Show("Error: Connection to the server is not established.");
                }

                // Wait for the server response here
                byte[] responseHeader = new byte[5];
                if (app.stream != null)
                {
                    app.stream.Read(responseHeader, 0, responseHeader.Length);
                }

                byte responseType = responseHeader[0];
                int jsonSize = BitConverter.ToInt32(responseHeader, 1);
                byte[] jsonResponse = new byte[jsonSize];
                if (app.stream != null)
                {
                    app.stream.Read(jsonResponse, 0, jsonResponse.Length);
                }
                serverTypeMsg = responseType;
                string serverJson = Encoding.UTF8.GetString(jsonResponse);
                dynamic jsonData = JsonConvert.DeserializeObject(serverJson);
                Dictionary<uint, string> answers = null;
                if (jsonData.answers != null)
                {
                    answers = jsonData.answers.ToObject<Dictionary<uint, string>>();
                }
                string question = null;
                if (jsonData.question != null)
                {
                    question = jsonData.question.ToString();
                }

                // Update UI elements directly
                questionTextBlock.Text = question;

                if (answers != null)
                {
                    foreach (KeyValuePair<uint, string> entry in answers)
                    {
                        uint key = entry.Key;
                        string answer = entry.Value;

                        Button button = new Button();
                        button.Content = answer;
                        button.Margin = new Thickness(0, 5, 0, 0);
                        button.Padding = new Thickness(10);
                        button.Width = 150;
                        button.Tag = key; // Store the key as the Tag property of the button
                        button.Click += AnswerButton_Click;

                        answersItemsControl.Items.Add(button);
                    }
                }
            }

        }

        private async void AnswerButton_Click(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;
            string selectedAnswer = button?.Content.ToString();
            int answerIndex = answersItemsControl.Items.IndexOf(button) + 1;
            uint answerNumber = (uint)answerIndex;

            // Create a JSON object with the key "answerId" and the value as the answerNumber
            var data = new
            {
                answerId = answerNumber
            };
            timer.Stop();
            isButtonPressed = true;

            if (Application.Current is App app && app.stream != null)
            {
                string json = JsonConvert.SerializeObject(data); // Convert the data to JSON
                byte[] jsonDataBytes = Encoding.UTF8.GetBytes(json);
                byte[] message = new byte[5 + jsonDataBytes.Length];

                // Set the message type as the first byte
                message[0] = (byte)16;

                // Set the length of the JSON data as the next four bytes
                byte[] lengthBytes = BitConverter.GetBytes(jsonDataBytes.Length);
                if (BitConverter.IsLittleEndian)
                    Array.Reverse(lengthBytes);
                Array.Copy(lengthBytes, 0, message, 1, 4);

                // Copy the JSON data bytes into the message
                Array.Copy(jsonDataBytes, 0, message, 5, jsonDataBytes.Length);

                // Send the message to the server if the stream is not null
                if (app.stream != null)
                {
                    app.stream.Write(message, 0, message.Length);
                    app.stream.Flush();
                }
                else
                {
                    MessageBox.Show("Error: Connection to the server is not established.");
                }

                // Wait for the server response here
                byte[] responseHeader = new byte[5];
                if (app.stream != null)
                {
                    app.stream.Read(responseHeader, 0, responseHeader.Length);
                }

                byte responseType = responseHeader[0];
                int jsonSize = BitConverter.ToInt32(responseHeader, 1);
                byte[] jsonResponse = new byte[jsonSize];
                if (app.stream != null)
                {
                    app.stream.Read(jsonResponse, 0, jsonResponse.Length);
                }
                string serverJson = Encoding.UTF8.GetString(jsonResponse);
                dynamic jsonData = JsonConvert.DeserializeObject(serverJson);
                if (responseType == 20)
                {
                    uint correctAnswerId = 0;
                    if (jsonData != null)
                    {
                        correctAnswerId = Convert.ToUInt32(jsonData.correctAnswerId);
                    }
                    MessageBox.Show("The correct answer is " + correctAnswerId);
                }
            }

            // Wait for the timer to finish
            questionTextBlock.Text = "";

            // Clear the answer buttons
            answersItemsControl.Items.Clear();


            if (counter < NumOfQuestions)
            {
                counter++;
                SendMessageToServer();
                RestartTimer();
            }
            else
            {
                ((App)Application.Current).SendMessageToServer(17, null!, null!);
            }
        }
        private void RestartTimer()
        {
            timer.Dispose(); // Dispose the current timer

            // Create a new timer with 10-second interval
            timer = new Timer(10000);
            timer.Elapsed += Timer_Elapsed;
            timer.Start();
        }
        private void LeaveGame_Click(object sender, RoutedEventArgs e)
        {
            ((App)Application.Current).SendMessageToServer(14, null!, null!);
            Room roomWin = new Room(playersJson, roomData);
            roomWin.Show();
            Close();
        }
    }
}