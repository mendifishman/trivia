﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Newtonsoft.Json.Linq;

namespace try_client._01
{
    /// <summary>
    /// Interaction logic for HighScores.xaml
    /// </summary>
    public partial class HighScores : Window
    {
        public HighScores(string serverJson)
        {
            InitializeComponent();
            JObject data = JObject.Parse(serverJson);
            JArray? statistics = (JArray?)data["statistics"];
            if (statistics != null)
            {
                foreach (var stat in statistics)
                {
                    string statText = stat.ToString(); // Modify this line based on your desired format
                    StatisticsListBox.Items.Add(statText);
                }
            }
        }
    }
}
