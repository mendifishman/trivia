﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Newtonsoft.Json.Linq;

namespace try_client._01
{
    public partial class PersonalStats : Window
    {
        public PersonalStats(string serverJson)
        {
            InitializeComponent();

            // Parse the serverJson into a dictionary or object to access the relevant information
            JObject data = JObject.Parse(serverJson);
            JArray? statistics = (JArray?)data["statistics"];

            // Display the statistics in the UI
            int i = 0;
            if (statistics != null)
            {
                foreach (var stat in statistics)
                {
                    string statText = stat.ToString(); // Modify this line based on your desired format
                    switch (i)
                    {
                        case 0:
                            statText = "Num of correct answers: " + statText;
                            break;
                        case 1:
                            statText = "Num of games played: " + statText;
                            break;
                        case 2:
                            statText = "Num of questions answered: " + statText;
                            break;
                        case 3:
                            statText = "Average answer time: " + statText;
                            break;
                        case 4:
                            statText = "Score: " + statText;
                            break;
                    }
                    StatisticsListBox.Items.Add(statText);
                    i++;
                }
            }
        }
    }
}
