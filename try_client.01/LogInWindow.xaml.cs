﻿using System.Windows;
using System.Windows.Controls;

namespace try_client._01
{
    public partial class LogInWindow : Page
    {
        public LogInWindow()
        {
            InitializeComponent();
        }

        private void Login_Click(object sender, RoutedEventArgs e)
        {
            string username = usernameTextBox.Text;
            string password = passwordBox.Password;

            // Create the JSON object for login
            var loginData = new
            {
                username = username,
                password = password
            };

            // Save the username in the App class
            ((App)Application.Current).Username = username;

            // Send the login JSON object to the server
            ((App)Application.Current).SendMessageToServer(1, loginData, null!);
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            // Stop the program execution
            Application.Current.Shutdown();
        }
    }
}
