﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Markup;
using Newtonsoft.Json;
using try_client._01;

namespace try_client
{
    public partial class Room : Window
    {
        private Thread updateThread;
        private bool isUpdateThreadRunning;

        // Declare a private field for the room object
        private RoomData room;
        private ManualResetEvent threadEvent = new ManualResetEvent(true);
        private string theJsonPlayer;
        private object theRoomData;
        public Room(string playersJson, object roomData)
        {
            theJsonPlayer = playersJson;
            theRoomData = roomData;
            InitializeComponent();

            // Deserialize the playersJson and extract the players
            dynamic jsonData = JsonConvert.DeserializeObject(playersJson);
            List<string> players = jsonData.players.ToObject<List<string>>();

            // Modify the players list
            if (players.Count > 0)
            {
                players[0] = "admin: " + players[0];
                if (players.Count > 1)
                {
                    string playersPrefix = "players: ";
                    string joinedPlayers = string.Join(", ", players.Skip(1));
                    players[1] = playersPrefix + joinedPlayers;
                    players.RemoveRange(2, players.Count - 2);
                }
            }

            // Set the modified players list as the data source for the ListBox
            playersListBox.ItemsSource = players;

            // Set the room object
            room = (RoomData)roomData;

            // Display room data
            roomDataTextBlock.Text = $"Room name: {room.Name}\n" +
                                      $"Max players: {room.MaxPlayers}\n" +
                                      $"Number of questions: {room.NumOfQuestions}\n" +
                                      $"Time per question: {room.TimePerQuestion} seconds";

            // Start the update thread
            StartUpdateThread();
        }


        private void StartUpdateThread()
        {
            // Create a new thread for updating the player list
            updateThread = new Thread(UpdatePlayerListThread);
            isUpdateThreadRunning = true;
            updateThread.Start();
        }

        private void UpdatePlayerListThread()
        {
            while (isUpdateThreadRunning)
            {
                // Wait until the threadEvent is signaled
                threadEvent.WaitOne();

                // Perform the player list update
                UpdatePlayerList();

                // Delay for 3 seconds
                Thread.Sleep(3000);
            }
        }


        private void UpdatePlayerList()
        {
            try
            {
                // Create the message to request an updated player list
                //byte[] message = new byte[5];
                //message[0] = 12; // Set the message type as 12

                // Send the message to the server if the stream is not null
                if (Application.Current is App app && app.stream != null)
                {
                    lock (app.stream) // Use lock to ensure thread safety
                    {
                        string json = JsonConvert.SerializeObject(null); // Convert the data to JSON
                        byte[] jsonDataBytes = Encoding.UTF8.GetBytes(json);
                        byte[] message = new byte[5 + jsonDataBytes.Length];

                        // Set the message type as the first byte
                        message[0] = (byte)12;

                        // Set the length of the JSON data as the next four bytes
                        byte[] lengthBytes = BitConverter.GetBytes(jsonDataBytes.Length);
                        if (BitConverter.IsLittleEndian)
                            Array.Reverse(lengthBytes);
                        Array.Copy(lengthBytes, 0, message, 1, 4);

                        // Copy the JSON data bytes into the message
                        Array.Copy(jsonDataBytes, 0, message, 5, jsonDataBytes.Length);

                        // Send the message to the server if the stream is not null
                        if (app.stream != null)
                        {
                            app.stream.Write(message, 0, message.Length);
                            app.stream.Flush();
                        }
                        else
                        {
                            MessageBox.Show("Error: Connection to the server is not established.");
                        }

                        // Wait for the server response here
                        byte[] responseHeader = new byte[5];
                        if (app.stream != null)
                        {
                            app.stream.Read(responseHeader, 0, responseHeader.Length);
                        }

                        byte responseType = responseHeader[0];
                        int jsonSize = BitConverter.ToInt32(responseHeader, 1);
                        byte[] jsonResponse = new byte[jsonSize];
                        if (app.stream != null)
                        {
                            app.stream.Read(jsonResponse, 0, jsonResponse.Length);
                        }

                        if (responseType == 20)
                        {
                            string serverJson = Encoding.UTF8.GetString(jsonResponse);
                            // Update the player list with the new data on the UI thread
                            Application.Current.Dispatcher.Invoke(() =>
                            {
                                UpdatePlayerListOnUI(serverJson);
                            });
                        }
                        else
                        {
                            // Stop the update thread and display a message box
                            isUpdateThreadRunning = false;
                            updateThread.Join();
                            MessageBox.Show("The game ended because the admin left the room!");

                            // Activate the logout button
                            logoutButton.IsEnabled = true;
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Error: Connection to the server is not established.");
                }
            }
            catch (Exception ex)
            {
                // Handle the exception appropriately
                //MessageBox.Show("Error updating player list: " + ex.Message);
            }
        }

        private void UpdatePlayerListOnUI(string serverJson)
        {
            dynamic jsonData = JsonConvert.DeserializeObject(serverJson);
            List<string> players = null;
            if (jsonData.players != null)
            {
                players = jsonData.players.ToObject<List<string>>();
            }
            if ((bool)jsonData.hasGameBegun)
            {
                //Close the Room window
                //threadEvent.Reset();
                isUpdateThreadRunning = false;
                TriviaGame triviaGame = new TriviaGame(room.TimePerQuestion, room.NumOfQuestions, theJsonPlayer, theRoomData);
                triviaGame.Show();
                //Close();
            }
            else if (players.Count > 0)
            {
                players[0] = "admin: " + players[0];
                if (players.Count > 1)
                {
                    string playersPrefix = "players: ";
                    string joinedPlayers = string.Join(", ", players.Skip(1));
                    players[1] = playersPrefix + joinedPlayers;
                    players.RemoveRange(2, players.Count - 2);
                }
            }

            playersListBox.ItemsSource = players;
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            // Stop the update thread when the window is closed
            isUpdateThreadRunning = false;
            updateThread.Join();
        }
        private void LogoutButton_Click(object sender, RoutedEventArgs e)
        {
            // Stop the update thread
            isUpdateThreadRunning = false;
            updateThread.Join();

            // Close the Room window
            Close();
            //((App)Application.Current).SendMessageToServer(13, null, null);
        }

        private void StartGameButton_Click(object sender, RoutedEventArgs e)
        {
            threadEvent.Reset();
            ((App)Application.Current).SendMessageToServer(11, null, null);

            // Check the actual response from the server
            int messageType = ((App)Application.Current).MessageType;
            if (messageType == 20)
            {
                isUpdateThreadRunning = false;
                updateThread.Join();

                // Close the Room window
                Close();
                TriviaGame triviaGame = new TriviaGame(room.TimePerQuestion, room.NumOfQuestions, theJsonPlayer, theRoomData);
                triviaGame.Show();
            }
            else
            {
                threadEvent.Set();
                MessageBox.Show("Unexpected server response.");
            }
        }



    }
}
