﻿using Newtonsoft.Json;

namespace try_client._01
{
    public class RoomData
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string? Name { get; set; }

        [JsonProperty("maxPlayers")]
        public int MaxPlayers { get; set; }

        [JsonProperty("numOfQuestionsInGame")]
        public int NumOfQuestions { get; set; }

        [JsonProperty("timePerQuestion")]
        public int TimePerQuestion { get; set; }

        [JsonProperty("isActive")]
        public bool IsActive { get; set; }
    }
}
