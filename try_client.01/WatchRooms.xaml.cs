﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Windows;

namespace try_client._01
{
    public partial class WatchRooms : Window
    {
        private Thread updateThread;
        private bool isUpdateThreadRunning;

        public WatchRooms(string serverJson)
        {
            InitializeComponent();

            var response = new
            {
                rooms = new List<RoomData>()
            };

            try
            {
                response = JsonConvert.DeserializeAnonymousType(serverJson, response);
                List<RoomData> rooms = response.rooms;

                roomsListView.ItemsSource = rooms;
            }
            catch (JsonReaderException)
            {
                MessageBox.Show("Error: Invalid server response.");
            }

            StartUpdateThread();
        }

        private void StartUpdateThread()
        {
            updateThread = new Thread(UpdatePlayerListThread);
            isUpdateThreadRunning = true;
            updateThread.Start();
        }

        private void UpdatePlayerListThread()
        {
            while (isUpdateThreadRunning)
            {
                UpdatePlayerList();
                Thread.Sleep(3000);
            }
        }

        private void UpdatePlayerList()
        {
            try
            {
                if (Application.Current is App app && app.stream != null)
                {
                    lock (app.stream)
                    {
                        string json = JsonConvert.SerializeObject(null);
                        byte[] jsonDataBytes = Encoding.UTF8.GetBytes(json);
                        byte[] message = new byte[5 + jsonDataBytes.Length];

                        message[0] = (byte)4;

                        byte[] lengthBytes = BitConverter.GetBytes(jsonDataBytes.Length);
                        if (BitConverter.IsLittleEndian)
                            Array.Reverse(lengthBytes);
                        Array.Copy(lengthBytes, 0, message, 1, 4);

                        Array.Copy(jsonDataBytes, 0, message, 5, jsonDataBytes.Length);

                        if (app.stream != null)
                        {
                            app.stream.Write(message, 0, message.Length);
                            app.stream.Flush();
                        }
                        else
                        {
                            MessageBox.Show("Error: Connection to the server is not established.");
                        }

                        byte[] responseHeader = new byte[5];
                        if (app.stream != null)
                        {
                            app.stream.Read(responseHeader, 0, responseHeader.Length);
                        }

                        byte responseType = responseHeader[0];
                        int jsonSize = BitConverter.ToInt32(responseHeader, 1);
                        byte[] jsonResponse = new byte[jsonSize];
                        if (app.stream != null)
                        {
                            app.stream.Read(jsonResponse, 0, jsonResponse.Length);
                        }

                        if (responseType == 20)
                        {
                            string serverJson = Encoding.UTF8.GetString(jsonResponse);
                            Application.Current.Dispatcher.Invoke(() =>
                            {
                                UpdatePlayerListOnUI(serverJson);
                            });
                        }
                        else
                        {
                            MessageBox.Show("Error: Server response was not as expected.");
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Error: Connection to the server is not established.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error updating player list: " + ex.Message);
            }
        }

        private void UpdatePlayerListOnUI(string serverJson)
        {
            var response = new
            {
                rooms = new List<RoomData>()
            };

            try
            {
                response = JsonConvert.DeserializeAnonymousType(serverJson, response);
                List<RoomData> rooms = response.rooms;

                roomsListView.ItemsSource = rooms;
            }
            catch (JsonReaderException)
            {
                MessageBox.Show("Error: Invalid server response.");
            }
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            isUpdateThreadRunning = false;
            updateThread.Join();
        }

        private void JoinRoom_Click(object sender, RoutedEventArgs e)
        {
            // Stop the update thread
            isUpdateThreadRunning = false;
            updateThread.Join();

            // Get the selected room from the ListView
            RoomData selectedRoom = (RoomData)roomsListView.SelectedItem;

            if (selectedRoom != null)
            {
                // Create the JSON object
                var json = new
                {
                    roomId = selectedRoom.Id
                };

                // Get the reference to the App object
                App app = (App)Application.Current;

                // Send the message to the server
                app.SendMessageToServer(6, json, selectedRoom);
                Close();
            }
            else
            {
                MessageBox.Show("Please select a room to join.");
            }
        }

    }
}
