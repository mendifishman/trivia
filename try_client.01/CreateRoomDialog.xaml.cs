﻿using System.Windows;

namespace try_client
{
    public partial class CreateRoomDialog : Window
    {
        public string? RoomName { get; private set; }
        public int? NumOfPlayers { get; private set; }
        public int? TimeOfQuestion { get; private set; }
        public int? NumOfQuestions { get; private set; }

        public CreateRoomDialog()
        {
            InitializeComponent();
        }

        private void Create_Click(object sender, RoutedEventArgs e)
        {
            // Retrieve the values from the input fields
            string? roomName = txtRoomName.Text;
            int numOfPlayers;
            int timeOfQuestion;
            int numOfQuestions;

            // Perform validation on the input values
            if (!string.IsNullOrEmpty(roomName) &&
                int.TryParse(txtNumOfPlayers.Text, out numOfPlayers) &&
                int.TryParse(txtTimeOfQuestion.Text, out timeOfQuestion) &&
                int.TryParse(txtNumOfQuestions.Text, out numOfQuestions))
            {
                // Set the property values
                RoomName = roomName;
                NumOfPlayers = numOfPlayers;
                TimeOfQuestion = timeOfQuestion;
                NumOfQuestions = numOfQuestions;

                // Close the dialog
                DialogResult = true;
            }
            else
            {
                MessageBox.Show("Error: Please enter valid room information.");
            }
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            // Close the dialog
            DialogResult = false;
        }
    }
}