﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace try_client._01
{
    /// <summary>
    /// Interaction logic for StatisticsRoom.xaml
    /// </summary>
    public partial class StatisticsRoom : Window
    {
        private App app;

        public StatisticsRoom(App app)
        {
            InitializeComponent();
            this.app = app;
        }

        private void HighScores_Click(object sender, RoutedEventArgs e)
        {
            app.SendMessageToServer(8, null!, null!);
        }

        private void PersonalStatistic_Click(object sender, RoutedEventArgs e)
        {
            app.SendMessageToServer(7, null!, null!);
        }
    }
}
