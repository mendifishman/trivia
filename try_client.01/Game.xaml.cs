﻿using System;
using System.Windows;

namespace try_client._01
{
    public partial class Game : Window
    {
        public Game()
        {
            InitializeComponent();
        }

        private void CreateRoom_Click(object sender, RoutedEventArgs e)
        {
            // Show a dialog to get the room information
            CreateRoomDialog dialog = new CreateRoomDialog();
            if (dialog.ShowDialog() == true)
            {
                // Retrieve the entered room information
                string? roomName = dialog.RoomName;
                int? numOfPlayers = dialog.NumOfPlayers;
                int? timeOfQuestion = dialog.TimeOfQuestion;
                int? numOfQuestions = dialog.NumOfQuestions;

                // Check if roomName is null before using it
                if (roomName != null && numOfPlayers != null && timeOfQuestion != null && numOfQuestions != null)
                {
                    // Create the JSON object
                    var roomData = new
                    {
                        roomName = roomName,
                        maxUsers = numOfPlayers,
                        answerTimeout = timeOfQuestion,
                        questionCount = numOfQuestions
                    };

                    // Create the RoomData object
                    var selectedRoom = new RoomData
                    {
                        Name = roomName,
                        MaxPlayers = numOfPlayers.Value,
                        NumOfQuestions = numOfQuestions.Value,
                        TimePerQuestion = timeOfQuestion.Value
                    };

                    // Send the roomData JSON object to the server
                    ((App)Application.Current).SendMessageToServer(3, roomData, selectedRoom);
                }
                else
                {
                    // Handle the case where any of the room information is null
                    MessageBox.Show("Error: Room information is incomplete.");
                }
            }
        }


        private void JoinRoom_Click(object sender, RoutedEventArgs e)
        {
            ((App)Application.Current).SendMessageToServer(4, null!, null!);
        }

        private void Statistics_Click(object sender, RoutedEventArgs e)
        {
            StatisticsRoom statisticsRoom = new StatisticsRoom((App)Application.Current);
            statisticsRoom.Show();
        }
        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            // Exit the program
            //((App)Application.Current).SendMessageToServer(9, null!, null!);
            Application.Current.Shutdown();
        }
    }
}