import socket
import struct
import json

SERVER_IP = "127.0.0.1"
SERVER_PORT = 5030

def take_info_from_user(logOrSign):
    print(logOrSign)
    userName = input("\nEnter your username: ")
    password = input("Enter your password: ")
    json_data = {'username': userName, 'password': password}
    if logOrSign == 2:
        mail_addr = input("Enter your email address: ")
        json_data['email'] = mail_addr
    return json.dumps(json_data)

def main():
    try:
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server_address = (SERVER_IP, SERVER_PORT)
        sock.connect(server_address)
    except Exception as e:
        print(e)
    while True:
        logOrSign = int(input("For login '1' and for sign up '2': "))
        json_data = take_info_from_user(logOrSign)
        json_data_encoded = json_data.encode()
        msg = struct.pack('!BI', logOrSign, len(json_data_encoded)) + json_data_encoded
        sock.sendall(msg)

    # more code here

if __name__ == "__main__":
    main()
