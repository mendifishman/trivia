#pragma once
#include "IResponse.h"
#include <vector>
#include <nlohmann/json.hpp>
using json = nlohmann::json;
typedef std::vector<unsigned char> Buffer;
class JsonResponsePacketSerializer
{
public:
	static Buffer serializeResponse(LoginResponse answer);
	static Buffer serializeResponse(SignupResponse answer);
	static Buffer serializeResponse(ErrorResponse answer);
	static json getJson();
	static Buffer encodeMsg(unsigned char code,json _json);
	static Buffer serializeResponse(LogoutResponse);
	static Buffer serializeResponse(GetRoomsResponse);
	static Buffer serializeResponse(GetPlayersInRoomResponse);
	static Buffer serializeResponse(JoinRoomResponse);
	static Buffer serializeResponse(CreateRoomResponse);
	static Buffer serializeResponse(getHighScoreResponse);
	static Buffer serializeResponse(getPersonalStatsResponse);
	static Buffer serializeResponse(CloseRoomResponse);
	static Buffer serializeResponse(StartGameResponse);
	static Buffer serializeResponse(GetRoomStateResponse);
	static Buffer serializeResponse(LeaveRoomResponse);
	static Buffer serializeResponse(GetGameResultsResponse);
	static Buffer serializeResponse(SubmitAnswerResponse);
	static Buffer serializeResponse(GetQuestionResponse);
	static Buffer serializeResponse(LeaveGameResponse);
};

