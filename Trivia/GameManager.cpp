#include "GameManager.h"

Game& GameManager::createGame(Room r)
{
	Game new_game;
	RoomData rd = r.get_data();
	new_game.m_gameId = rd.id;
	std::list<Question> lq =  this->m_database->getQuestions(rd.numOfQuestionsInGame);
	new_game.m_questions = std::vector<Question>(lq.begin(), lq.end());
	for (auto var : r.getAllUsers())
	{
		GameData gd;
		gd.averangeAnswerTime = 0;
		gd.correctAnswerCount = 0;
		gd.wrongAnswerCount = 0;
		new_game.m_players.insert(std::pair<LoggedUser, GameData>(LoggedUser(var), gd));
	}
	LoggedUser admin = r.getAllUsers()[0];
	new_game.m_database = this->m_database;
	this->m_games.insert(std::pair<LoggedUser, Game>(admin, new_game));
	return this->m_games.at(admin);
}

void GameManager::deleteGame(unsigned int id)
{
	auto it = m_games.begin();
	while (it != m_games.end()) {
		if (it->second.m_gameId == id) {
			for (const auto& player : it->second.m_players) {
				PlayerResults p;
				p.averageAnswerTime = player.second.averangeAnswerTime;
				p.correctAnswerCount = player.second.correctAnswerCount;
				p.wrongAnswerCount = player.second.wrongAnswerCount;
				p.username = player.first.getUsername();
				m_database->insertStatistics(p);
			}
			it = m_games.erase(it);
		}
		else {
			++it;
		}
	}
}


Game& GameManager::getGame(LoggedUser user)
{
	for (auto& game : m_games)
	{
		if (game.second.m_players.find(user) != game.second.m_players.end())
		{
			return game.second;
		}
	}
	// If no match is found, throw an exception or handle the case accordingly
	throw std::runtime_error("Game not found for the specified user.");
}

Game& GameManager::getGame(unsigned int id)
{
	for (auto& game : m_games)
	{
		if (game.second.m_gameId == id)
		{
			return game.second;
		}
	}
}
