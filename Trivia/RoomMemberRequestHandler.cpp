#include "RoomMemberRequestHandler.h"
#include "RequestHandlerFactory.h"
#include "JsonResponsePacketSerializer.h"

bool RoomMemberRequestHandler::isRequestRelevant(RequestInfo r)
{
    return r.id == (unsigned int)code::LEAVE_ROOM || r.id == (unsigned int)code::GET_ROOM_STATE;
}

RequestResult RoomMemberRequestHandler::handleRequest(RequestInfo r)
{
    code c = (code)r.id;
    RequestResult request_result;
    switch (c)
    {
    case code::LEAVE_ROOM:
        request_result = this->leaveRoom(r);
        break;
    case code::GET_ROOM_STATE:
        request_result = this->getRoomState(r);
        break;
    default:
        break;
    }
    return request_result;
}

RequestResult RoomMemberRequestHandler::leaveRoom(RequestInfo)
{
    this->m_roomManager.getRoom(this->m_room.get_data().id).removeUser(this->m_user);
    return RequestResult{ JsonResponsePacketSerializer::serializeResponse(LeaveRoomResponse{(unsigned int)code::OK}),this->m_handlerFactory.createMenuRequestHandler(this->m_user) };
}

RequestResult RoomMemberRequestHandler::getRoomState(RequestInfo)
{
    GetRoomStateResponse r;
    RoomData rd;
    try
    {
        rd = this->m_roomManager.getRoom(this->m_room.get_data().id).get_data();
        r._answerTimeout = rd.timePerQuestion;
        r._hasGameBegun = rd.isActive;
        r._players = this->m_roomManager.getRoom(this->m_room.get_data().id).getAllUsers();
        r._questionCount = rd.numOfQuestionsInGame;
        r._status = (unsigned int)code::OK;
    }
    catch (...)
    {
        return RequestResult{ JsonResponsePacketSerializer::serializeResponse(LeaveRoomResponse{(unsigned int)code::OK}),this->m_handlerFactory.createMenuRequestHandler(this->m_user) };
    }
    if (rd.isActive == true)
    {
        return RequestResult{ JsonResponsePacketSerializer::serializeResponse(GetRoomStateResponse{(unsigned int)code::OK,r._hasGameBegun,r._players,r._questionCount,r._answerTimeout}),this->m_handlerFactory.createGameRequestHandler(this->m_user) };
    }
    return RequestResult{ JsonResponsePacketSerializer::serializeResponse(GetRoomStateResponse{(unsigned int)code::OK,r._hasGameBegun,r._players,r._questionCount,r._answerTimeout}),this->m_handlerFactory.createRoomMemberRequestHandler(this->m_user,this->m_room) };
}
