#include "GameRequestHandler.h"
#include "JsonRequestPacketDeserializer.h"
#include "JsonResponsePacketSerializer.h"
#include "RequestHandlerFactory.h"
#include <algorithm>

bool GameRequestHandler::isRequestRelevant(RequestInfo r)
{
    return r.id == (unsigned int)code::LEAVE_GAME || r.id == (unsigned int)code::GET_QUESTION || r.id == (unsigned int)code::SUBMIT_ANSWER || r.id == (unsigned int)code::GET_GAME_RESULT;
}

RequestResult GameRequestHandler::handleRequest(RequestInfo r)
{
    code c = (code)r.id;
    RequestResult request_result;
    switch (c)
    {
    case code::LEAVE_GAME:
        request_result = this->leaveGame(r);
        break;
    case code::GET_QUESTION:
        request_result = this->getQuestion(r);
        break;
    case code::SUBMIT_ANSWER:
        request_result = this->submitAnswer(r);
        break;
    case code::GET_GAME_RESULT:
        request_result = this->getGameResults(r);
        break;
    default:
        break;
    }
    return request_result;
}

RequestResult GameRequestHandler::getQuestion(RequestInfo)
{
    std::map<unsigned int, std::string> answersMap;
    for (unsigned int i = 0; i < this->m_gameManager.getGame(this->m_user).getQuestionForUser(this->m_user).answers.size(); i++) {
        answersMap[i] = this->m_gameManager.getGame(this->m_user).getQuestionForUser(this->m_user).answers[i];
    }
    return RequestResult{ JsonResponsePacketSerializer::serializeResponse(GetQuestionResponse{(unsigned int)code::OK,this->m_gameManager.getGame(this->m_user).getQuestionForUser(this->m_user).questoin_text,answersMap}), this->m_handlerFacroty.createGameRequestHandler(this->m_user) };
}

RequestResult GameRequestHandler::submitAnswer(RequestInfo r)
{
    unsigned int id = JsonRequestPacketDeserializer::deserializeSubmitAnswerRequest(r.b).answerId;
    this->m_gameManager.getGame(this->m_user).submitAnswer(this->m_user, id, r.receivalTime);
    return RequestResult{ JsonResponsePacketSerializer::serializeResponse(SubmitAnswerResponse{(unsigned int)code::OK,this->m_gameManager.getGame(this->m_user).getQuestionForUser(this->m_user).corrent_answer}), this->m_handlerFacroty.createGameRequestHandler(this->m_user) };
}

bool comparePlayerResults(const PlayerResults& a, const PlayerResults& b)
{
    // Sort by correctAnswerCount in descending order
    if (a.correctAnswerCount != b.correctAnswerCount)
        return a.correctAnswerCount > b.correctAnswerCount;

    // If correctAnswerCount is the same, sort by wrongAnswerCount in ascending order
    if (a.wrongAnswerCount != b.wrongAnswerCount)
        return a.wrongAnswerCount < b.wrongAnswerCount;

    // If both counts are the same, sort by averageAnswerTime in ascending order
    return a.averageAnswerTime < b.averageAnswerTime;
}

RequestResult GameRequestHandler::getGameResults(RequestInfo)
{
    std::vector<PlayerResults> players;
    while (true)
    {
        if (this->m_gameManager.getGame(this->m_user).is_finished() == true)
        {
            for (auto var : this->m_gameManager.getGame(this->m_user).m_players)
            {
                PlayerResults p;
                p.averageAnswerTime = var.second.averangeAnswerTime;
                p.correctAnswerCount = var.second.averangeAnswerTime;
                p.username = var.first.getUsername();
                p.wrongAnswerCount = var.second.wrongAnswerCount;
                players.push_back(p);
            }
            std::sort(players.begin(), players.end(), comparePlayerResults);

            return RequestResult{ JsonResponsePacketSerializer::serializeResponse(GetGameResultsResponse{(unsigned int)code::OK,players}), this->m_handlerFacroty.createGameRequestHandler(this->m_user) };
        }
    }
}

RequestResult GameRequestHandler::leaveGame(RequestInfo)
{
    LoggedUser admin("");
    roomID id = 0;
    for (auto rd : this->m_handlerFacroty.getRoomManager().getRooms())
    {
        Room room = this->m_handlerFacroty.getRoomManager().getRoom(rd.id);
        for (auto player : room.getAllUsers())
        {
            if (player == this->m_user.getUsername())
            {
                id = room.get_data().id;
                admin = room.getAllUsers()[0];
            }
        }
    }
    this->m_gameManager.getGame(this->m_user).removePlayer(this->m_user);
    if (this->m_gameManager.getGame(id).m_players.empty())
    {
        this->m_gameManager.deleteGame(this->m_game.m_gameId);
    }
    if (this->m_user.getUsername() == admin.getUsername())
    {
        return RequestResult{ JsonResponsePacketSerializer::serializeResponse(LeaveGameResponse{(unsigned int)code::OK}), this->m_handlerFacroty.createRoomAdminRequestHandler(this->m_user,this->m_handlerFacroty.getRoomManager().getRoom(id))};
    }
    else
    {
        return RequestResult{ JsonResponsePacketSerializer::serializeResponse(LeaveGameResponse{(unsigned int)code::OK}), this->m_handlerFacroty.createRoomMemberRequestHandler(this->m_user,this->m_handlerFacroty.getRoomManager().getRoom(id)) };
    }
}
