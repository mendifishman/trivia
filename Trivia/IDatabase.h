#pragma once
#include <string>
#include "sqlite3.h"
#include <vector>
#include <string>
#include <list>

typedef struct UserDB {
	std::string username;
	std::string password;
	std::string email;
} UserDB;
typedef struct Question {
	std::string questoin_text;
	std::vector<std::string> answers;
	unsigned int corrent_answer;
} Question;
typedef struct PlayerResults
{
	std::string username;
	unsigned int correctAnswerCount;
	unsigned int wrongAnswerCount;
	float averageAnswerTime;
}PlayerResults;
class IDatabase
{
public:
	IDatabase() = default;
	virtual ~IDatabase() = default;
	virtual void open() = 0;
	virtual void close() = 0;
	virtual void clear() = 0;
	virtual void add_user(UserDB) = 0;
	virtual bool does_user_exist(UserDB) = 0;
	virtual bool is_password_correct(UserDB) = 0;
	virtual std::list<Question> getQuestions(int) = 0;
	virtual float getPlayerAverageAnswerTime(std::string username) = 0;
	virtual int getNumOfCorrectAnswers(std::string username) = 0;
	virtual int getNumOfTotalAnswers(std::string username) = 0;
	virtual int getNumOfPlayerGames(std::string username) = 0;
	virtual int getPlayerScore(std::string username) = 0;
	virtual std::vector<std::string> getHighScores() = 0;
	virtual void insertStatistics(PlayerResults) = 0;
	virtual void insertKey(std::string username, std::string key) = 0;
	virtual std::string getKey(std::string username) = 0;
};
