#pragma once
#include <iostream>
#include <vector>
#include <map>
#include "Room.h"
#include "IDatabase.h"
typedef struct LoginResponse
{
	unsigned int _status;
}LoginResponse;

typedef struct SignupResponse
{
	unsigned int _status;
}SignupResponse;

typedef struct ErrorResponse
{
	std::string _errMsg;
}ErrorResponse;

typedef struct LogoutResponse
{
	unsigned int _status;
}LogoutResponse;

typedef struct GetRoomsResponse
{
	unsigned int _status;
	std::vector<RoomData> _rooms;
}GetRoomsResponse;

typedef struct GetPlayersInRoomResponse
{
	std::vector<std::string> _playres;
}GetPlayersInRoomResponse;

typedef struct getHighScoreResponse
{
	unsigned int _status;
	std::vector<std::string> _statistics;
}getHighScoreResponse;

typedef struct getPersonalStatsResponse
{
	unsigned int _status;
	std::vector<std::string> _statistics;
}getPersonalStatsResponse;

typedef struct JoinRoomResponse
{
	unsigned int _status;
}JoinRoomResponse;

typedef struct CreateRoomResponse
{
	unsigned int _status;
}CreateRoomResponse;

typedef struct CloseRoomResponse
{
	unsigned int _status;
}CloseRoomResponse;

typedef struct StartGameResponse
{
	unsigned int _status;
}StartGameResponse;

typedef struct GetRoomStateResponse
{
	unsigned int _status;
	bool _hasGameBegun;
	std::vector<std::string> _players;
	unsigned int _questionCount;
	unsigned int _answerTimeout;
}GetRoomStateResponse;

typedef struct LeaveRoomResponse
{
	unsigned int _status;
}LeaveRoomResponse;



typedef struct LeaveGameResponse
{
	unsigned int _status;
}LeaveGameResponse;

typedef struct GetQuestionResponse
{
	unsigned int _status;
	std::string question;
	std::map<unsigned int, std::string> answers;
}GetQuestionResponse;

typedef struct SubmitAnswerResponse
{
	unsigned int _status;
	unsigned int correctAnswerId;
}SubmitAnswerResponse;

typedef struct GetGameResultsResponse
{
	unsigned int _status;
	std::vector<PlayerResults> results;
}GetGameResultsResponse;