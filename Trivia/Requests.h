#pragma once
#include <iostream>
#include <string>
#include <vector>
#include <chrono>
typedef std::vector<unsigned char> Buffer;

typedef struct LoginRequest {
	std::string username;
	std::string password;
} LoginRequest;

typedef struct SignupRequest {
	std::string username;
	std::string password;
	std::string email;
} SignupRequest;

typedef struct RequestInfo {
	unsigned int id;
	std::vector<unsigned char> b;
	std::chrono::system_clock::time_point receivalTime;
} RequestInfo;

class IRequestHandler;

typedef struct RequestResult {
	std::vector<unsigned char> b;
	IRequestHandler* new_handler;
} RequestResult;

typedef struct GetPlayersInRoomRequest {
	unsigned int roomId;
} GetPlayersInRoomRequest;

typedef struct JoinRoomRequest {
	unsigned int roomId;
} JoinRoomRequest;

typedef struct CreateRoomRequest
{
	std::string roomName;
	unsigned int maxUsers;
	unsigned int questionCount;
	unsigned int answerTimeout;
} CreateRoomRequest;

typedef struct SubmitAnswerRequest{
	unsigned int answerId;
} SubmitAnswerRequest;