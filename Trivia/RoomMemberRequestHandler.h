#pragma once

#include "IRequestHandler.h"
#include "LoginManager.h"
#include "RoomManager.h"
#include "Codes.h"

class RequestHandlerFactory;
class RoomMemberRequestHandler : public IRequestHandler
{
public:
	RoomMemberRequestHandler(LoggedUser u, RoomManager& rm, RequestHandlerFactory& h, Room r) : IRequestHandler(), m_user(u), m_roomManager(rm), m_handlerFactory(h), m_room(r)
	{
	}
	virtual bool isRequestRelevant(RequestInfo r) override;
	virtual RequestResult handleRequest(RequestInfo r) override;
	RequestResult leaveRoom(RequestInfo);
	RequestResult getRoomState(RequestInfo);
private:
	LoggedUser m_user;
	RoomManager& m_roomManager;
	RequestHandlerFactory& m_handlerFactory;
	Room m_room;
};