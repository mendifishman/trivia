#pragma once
int callback_does_exist(void* data, int argc, char** argv, char** azColName);
int does_nothing(void* data, int argc, char** argv, char** azColName);
int callback_get_password(void* data, int argc, char** argv, char** azColName);
int callback_get_question(void* data, int argc, char** argv, char** azColName);
int callback_get_time(void* data, int argc, char** argv, char** azColName);
int callback_get_num(void* data, int argc, char** argv, char** azColName);
int callback_get_high_scores(void* data, int argc, char** argv, char** azColName);
