#pragma once
#include "IDatabase.h"
#include <map>
#include "LoginManager.h"
#include "RoomManager.h"
#include <chrono>
typedef struct GameData
{
	Question currentQuestion;
	unsigned int correctAnswerCount;
	unsigned int wrongAnswerCount;
	float averangeAnswerTime;
	std::chrono::system_clock::time_point gotQuestion;
	long double totalTime;
}GameData;
class Game
{
public:
	Question getQuestionForUser(LoggedUser u)
	{
		if ((this->m_players.at(u).wrongAnswerCount + this->m_players.at(u).correctAnswerCount) < this->m_questions.size())
		{
			this->m_players.at(u).currentQuestion = this->m_questions[(this->m_players.at(u).wrongAnswerCount + this->m_players.at(u).correctAnswerCount)];
			this->m_players.at(u).gotQuestion = std::chrono::system_clock::now();
			return this->m_questions[(this->m_players.at(u).wrongAnswerCount + this->m_players.at(u).correctAnswerCount)];
		}
		else if ((this->m_players.at(u).wrongAnswerCount + this->m_players.at(u).correctAnswerCount) == this->m_questions.size())
		{
			return this->m_questions.back();
		}
		
	}
	void submitAnswer(LoggedUser u, unsigned int answerId, std::chrono::system_clock::time_point time)
	{
		for (auto var : this->m_questions)
		{
			if (var.questoin_text == this->m_players.at(u).currentQuestion.questoin_text)
			{
				GameData gd = this->m_players.at(u);
				if (answerId == this->m_players.at(u).currentQuestion.corrent_answer)
				{
					gd.correctAnswerCount++;
				}
				else
				{
					gd.wrongAnswerCount++;
				}
				std::chrono::duration<long double> elapsedTime = time - gd.gotQuestion;
				gd.totalTime = elapsedTime.count();
				gd.averangeAnswerTime = (float)(gd.totalTime / (gd.correctAnswerCount + gd.wrongAnswerCount));
				this->m_players.at(u) = gd;
			}
		}
	}
	void removePlayer(LoggedUser u)
	{
		this->m_players.erase(u);
	}
	bool is_finished()
	{
		bool is = true;
		for (auto var : m_players)
		{
			if ((var.second.wrongAnswerCount + var.second.wrongAnswerCount) != this->m_questions.size())
			{
				is = false;
			}
		}
		return is;
	}
	IDatabase* m_database;
	std::vector<Question> m_questions;
	std::map<LoggedUser, GameData> m_players;
	unsigned int m_gameId;
};
class GameManager
{
public:
	GameManager(IDatabase* d) : m_database(d)
	{
	}
	Game& createGame(Room);
	void deleteGame(unsigned int);
	Game& getGame(LoggedUser);
	Game& getGame(unsigned int id);
private:
	IDatabase* m_database;
	std::map<LoggedUser ,Game> m_games;
};