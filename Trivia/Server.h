#pragma once
using namespace std;
#include "Communicator.h"
#include "RequestHandlerFactory.h"
#include "IDatabase.h"
#define EXIT "EXIT"
#include "Helper.h"

class Server
{
public:
	Server();
	~Server();
	void run();
	void get_input();
private:
	SOCKET _serverSocket;
	Communicator m_com;
	IDatabase* m_database;
	RequestHandlerFactory m_handlerFactory;
};

