#pragma once
#include "Room.h"
#include <map>
typedef  unsigned int roomID;
class RoomManager
{
public:
	bool createRoom(LoggedUser, RoomData);
	bool deleteRoom(roomID ID);
	unsigned int getRoomState(roomID ID);
	std::vector<RoomData> getRooms();
	Room& getRoom(roomID ID);
	roomID getRoomID()
	{
		return _id;
	}
private:
	std::map<roomID, Room> m_rooms;
	static roomID _id;
};