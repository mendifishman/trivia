#pragma once
#include "IRequestHandler.h"
#include "Codes.h"
class RequestHandlerFactory;

class LoginRequestHandler : public IRequestHandler
{
public:
	LoginRequestHandler(RequestHandlerFactory& r) :IRequestHandler(), m_handlerFactory(r)
	{

	}
	virtual bool isRequestRelevant(RequestInfo r) override;
	virtual RequestResult handleRequest(RequestInfo r) override;
	RequestResult login(RequestInfo);
	RequestResult signup(RequestInfo);

private:
	RequestHandlerFactory& m_handlerFactory;
};