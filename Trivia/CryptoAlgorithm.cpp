#include "CryptoAlgorithm.h"
#include <random>
#include <ctime>
#include <cctype>

std::string OTPCryptoAlgorithm::encrypt(const std::string& message, const std::string& username)
{
    std::string key = m_database->getKey(username);
    std::string encrypted_msg;

    for (std::size_t i = 0; i < message.size(); i++)
    {
        char currentChar = message[i];
        if (std::islower(currentChar))
        {
            encrypted_msg.push_back((((currentChar - 'a') + (key[i] - 'a')) % 26) + 'a');
        }
        else if (std::isupper(currentChar))
        {
            encrypted_msg.push_back((((currentChar - 'A') + (std::toupper(key[i]) - 'A')) % 26) + 'A');
        }
        else
        {
            encrypted_msg.push_back(currentChar);
        }
    }

    return encrypted_msg;
}

std::string OTPCryptoAlgorithm::decrypt(const std::string& cypher, const std::string& username)
{
    std::string key = m_database->getKey(username);
    std::string decrypted_msg;

    for (std::size_t i = 0; i < cypher.size(); i++)
    {
        char currentChar = cypher[i];
        if (std::islower(currentChar))
        {
            decrypted_msg.push_back((((currentChar - 'a') - (key[i] - 'a') + 26) % 26) + 'a');
        }
        else if (std::isupper(currentChar))
        {
            decrypted_msg.push_back((((currentChar - 'A') - (std::toupper(key[i]) - 'A') + 26) % 26) + 'A');
        }
        else
        {
            decrypted_msg.push_back(currentChar);
        }
    }

    return decrypted_msg;
}

void OTPCryptoAlgorithm::generateKey(const std::string& username)
{
    std::srand(static_cast<unsigned int>(std::time(nullptr)));
    std::string generatedKey;
    generatedKey.reserve(MAX);
    for (int i = 0; i < MAX; i++)
    {
        char randomChar = 'a' + (std::rand() % 26);
        generatedKey.push_back(randomChar);
    }
    key = generatedKey;
    m_database->insertKey(username, key);
}