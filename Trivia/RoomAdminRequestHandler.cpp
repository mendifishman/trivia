#include "RoomAdminRequestHandler.h"
#include "RequestHandlerFactory.h"
#include "JsonRequestPacketDeserializer.h"
#include "JsonResponsePacketSerializer.h"

bool RoomAdminRequestHandler::isRequestRelevant(RequestInfo r)
{
    return r.id == (unsigned int)code::CLOSE_ROOM || r.id == (unsigned int)code::START_GAME || r.id == (unsigned int)code::GET_ROOM_STATE;
}

RequestResult RoomAdminRequestHandler::handleRequest(RequestInfo r)
{
    code c = (code)r.id;
    RequestResult request_result;
    switch (c)
    {
    case code::CLOSE_ROOM:
        request_result = this->closeRoom(r);
        break;
    case code::START_GAME:
        request_result = this->startGame(r);
        break;
    case code::GET_ROOM_STATE:
        request_result = this->getRoomState(r);
        break;
    default:
        break;
    }
    return request_result;
}

RequestResult RoomAdminRequestHandler::closeRoom(RequestInfo)
{
    this->m_roomManager.deleteRoom(this->m_room.get_data().id);
    return RequestResult{ JsonResponsePacketSerializer::serializeResponse(LeaveRoomResponse{(unsigned int)code::OK}), this->m_handlerFactory.createMenuRequestHandler(this->m_user)};
}

RequestResult RoomAdminRequestHandler::startGame(RequestInfo)
{
    RoomData r = this->m_roomManager.getRoom(this->m_room.get_data().id).get_data();
    r.isActive = true;
    this->m_roomManager.getRoom(this->m_room.get_data().id).set_data(r);
    this->m_handlerFactory.getGameManager().createGame(this->m_roomManager.getRoom(this->m_room.get_data().id));
    return RequestResult{ JsonResponsePacketSerializer::serializeResponse(StartGameResponse{(unsigned int)code::OK}),this->m_handlerFactory.createGameRequestHandler(this->m_user)};
}

RequestResult RoomAdminRequestHandler::getRoomState(RequestInfo)
{
    GetRoomStateResponse r;
    try
    {
        RoomData rd = this->m_roomManager.getRoom(this->m_room.get_data().id).get_data();
        r._answerTimeout = rd.timePerQuestion;
        r._hasGameBegun = rd.isActive;
        r._players = this->m_roomManager.getRoom(this->m_room.get_data().id).getAllUsers();
        r._questionCount = rd.numOfQuestionsInGame;
        r._status = (unsigned int)code::OK;
    }
    catch (...)
    {
        return RequestResult{ JsonResponsePacketSerializer::serializeResponse(LeaveRoomResponse{(unsigned int)code::OK}),this->m_handlerFactory.createMenuRequestHandler(this->m_user) };
    }
    return RequestResult{ JsonResponsePacketSerializer::serializeResponse(GetRoomStateResponse{(unsigned int)code::OK,r._hasGameBegun,r._players,r._questionCount,r._answerTimeout}),this->m_handlerFactory.createRoomAdminRequestHandler(this->m_user,this->m_room) };
}
