#include "IResponse.h"

IResponse::IResponse(unsigned int status) :
	_status(status)
{
}

IResponse::IResponse()
{
}

IResponse::~IResponse()
{
	_status = 0;
}

LoginResponse::LoginResponse(unsigned int status) :
	IResponse(status)
{
}


LoginResponse::~LoginResponse()
{
	IResponse::~IResponse();
}

SignupResponse::SignupResponse(unsigned int status):
	IResponse(status)
{

}


SignupResponse::SignupResponse()
{
	IResponse::~IResponse();
}

ErrorResponse::ErrorResponse(std::string errMsg) :
	_errMsg(errMsg)
{
}

ErrorResponse::ErrorResponse()
{
	_errMsg = "";
}
