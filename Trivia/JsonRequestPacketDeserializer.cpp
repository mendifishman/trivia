#include "JsonRequestPacketDeserializer.h"

LoginRequest JsonRequestPacketDeserializer::deserializeLoginRequest(Buffer b)
{
    std::string json_str(b.begin(), b.end());
    json j = json::parse(json_str);
    return LoginRequest{ j["username"], j["password"] };
}

SignupRequest JsonRequestPacketDeserializer::deserializeSignupRequest(Buffer b)
{
    std::string json_str(b.begin(), b.end());
    json j = json::parse(json_str);
    return SignupRequest{ j["username"], j["password"], j["email"] };
}

GetPlayersInRoomRequest JsonRequestPacketDeserializer::deserializeGetPlayersRequest(Buffer b)
{
    std::string json_str(b.begin(), b.end());
    json j = json::parse(json_str);
    return GetPlayersInRoomRequest{ j["roomId"] };
}

JoinRoomRequest JsonRequestPacketDeserializer::deserializeJoinRoomRequest(Buffer b)
{
    std::string json_str(b.begin(), b.end());
    json j = json::parse(json_str);
    return JoinRoomRequest{ j["roomId"] };
}

CreateRoomRequest JsonRequestPacketDeserializer::deserializeCreateRoomRequest(Buffer b)
{
    std::string json_str(b.begin(), b.end());
    json j = json::parse(json_str);
    return CreateRoomRequest{j["roomName"],j["maxUsers"],j["questionCount"],j["answerTimeout"]};
}

SubmitAnswerRequest JsonRequestPacketDeserializer::deserializeSubmitAnswerRequest(Buffer b)
{
    std::string json_str(b.begin(), b.end());
    json j = json::parse(json_str);
    return SubmitAnswerRequest{ j["answerId"] };
};

