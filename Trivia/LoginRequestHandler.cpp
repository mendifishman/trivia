#include "LoginRequestHandler.h"
#include "Requests.h"
#include "RequestHandlerFactory.h"
#include "JsonRequestPacketDeserializer.h"
#include "JsonResponsePacketSerializer.h"


bool LoginRequestHandler::isRequestRelevant(RequestInfo r)
{
    if (r.id == (int)code::LOG_IN || r.id == (int)code::SIGN_UP)
    {
        return true;
    }
    return false;
}

RequestResult LoginRequestHandler::handleRequest(RequestInfo r)
{
    if (r.id == (int)code::LOG_IN)
    {
        return this->login(r);
    }
    else if (r.id == (int)code::SIGN_UP)
    {
        return this->signup(r);
    }
}

RequestResult LoginRequestHandler::login(RequestInfo r)
{
    RequestResult rqr;
    LoginRequest login_rq = JsonRequestPacketDeserializer::deserializeLoginRequest(r.b);
    if (this->m_handlerFactory.getLoginManager().login(login_rq.username, login_rq.password) == true)
    {
        LoginResponse r;
        r._status = (unsigned char)code::OK;
        rqr.b = JsonResponsePacketSerializer::serializeResponse(r);
        rqr.new_handler = this->m_handlerFactory.createMenuRequestHandler(LoggedUser(login_rq.username));
    }
    else
    {
        LoginResponse r;
        r._status = (unsigned char)code::NOT_OK;
        rqr.b = JsonResponsePacketSerializer::serializeResponse(r);
        rqr.new_handler = this->m_handlerFactory.createLoginRequestHandler();
    }
    return rqr;
}

RequestResult LoginRequestHandler::signup(RequestInfo r)
{
    RequestResult rqr;
    SignupRequest signup_rq= JsonRequestPacketDeserializer::deserializeSignupRequest(r.b);
    if (this->m_handlerFactory.getLoginManager().signup(signup_rq.username, signup_rq.password, signup_rq.email) == true)
    {
        SignupResponse r;
        r._status = (unsigned char)code::OK;
        rqr.b = JsonResponsePacketSerializer::serializeResponse(r);
        rqr.new_handler = this->m_handlerFactory.createMenuRequestHandler(LoggedUser(signup_rq.username));
    }
    else
    {
        SignupResponse r;
        r._status = (unsigned char)code::NOT_OK;
        rqr.b = JsonResponsePacketSerializer::serializeResponse(r);
        rqr.new_handler = this->m_handlerFactory.createLoginRequestHandler();
    }
    return rqr;
}
