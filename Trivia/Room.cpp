#include "Room.h"
#include <algorithm>
bool Room::addUser(LoggedUser u)
{
	this->m_users.push_back(u);
	return true;
}
bool Room::removeUser(LoggedUser u)
{
	auto it = std::remove(this->m_users.begin(), this->m_users.end(), u);
	this->m_users.erase(it, this->m_users.end());
	return true;
}
std::vector<std::string> Room::getAllUsers()
{
	std::vector<std::string> user_list;
	std::for_each(this->m_users.begin(), this->m_users.end(), [&](LoggedUser u) {user_list.push_back(u.getUsername()); });
	return user_list;
}