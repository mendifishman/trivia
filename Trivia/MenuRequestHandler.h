#pragma once

#include "IRequestHandler.h"
#include "LoginManager.h"
#include "RoomManager.h"
#include "StatisticsManager.h"
#include "Codes.h"

class RequestHandlerFactory;
class MenuRequestHandler : public IRequestHandler
{
public:
	MenuRequestHandler(LoggedUser u, RoomManager& r, RequestHandlerFactory& h, StatisticsManager& s) : IRequestHandler(), m_user(u), m_roomManager(r), m_handlerFactory(h), m_statisticsManager(s)
	{
	}
	virtual bool isRequestRelevant(RequestInfo r) override;
	virtual RequestResult handleRequest(RequestInfo r) override;
	RequestResult signout(RequestInfo);
	RequestResult getRooms(RequestInfo);
	RequestResult getPlayersInRoom(RequestInfo);
	RequestResult getPersonalStats(RequestInfo);
	RequestResult getHighScore(RequestInfo);
	RequestResult joinRoom(RequestInfo);
	RequestResult createRoom(RequestInfo);

private:
	LoggedUser m_user;
	RoomManager& m_roomManager;
	RequestHandlerFactory& m_handlerFactory;
	StatisticsManager& m_statisticsManager;
};