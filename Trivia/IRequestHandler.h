#pragma once

#include "IResponse.h"
#include "Requests.h"


class IRequestHandler
{
public:
	IRequestHandler()
	{

	}
	virtual bool isRequestRelevant(RequestInfo) = 0;
	virtual RequestResult handleRequest(RequestInfo) = 0;
};