#pragma once
#include "Helper.h"
#include "LoginManager.h"
typedef struct RoomData
{
	unsigned int id;
	std::string name;
	unsigned int maxPlayers;
	unsigned int numOfQuestionsInGame;
	unsigned int timePerQuestion;
	unsigned int isActive;
	bool is_deleted;
}RoomData;
class Room
{
public:
	bool addUser(LoggedUser);
	bool removeUser(LoggedUser);
	std::vector<std::string> getAllUsers();
	void set_data(RoomData r)
	{
		this->m_metadata = r;
	}
	RoomData get_data()
	{
		return this->m_metadata;
	}
private:
	RoomData m_metadata;
	std::vector<LoggedUser> m_users;


};