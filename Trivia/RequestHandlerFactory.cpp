#include "RequestHandlerFactory.h"



LoginRequestHandler* RequestHandlerFactory::createLoginRequestHandler()
{
	LoginRequestHandler* l = new LoginRequestHandler(*this);
	return l;
}


LoginManager& RequestHandlerFactory::getLoginManager()
{
	return this->m_loginManager;
}

RoomAdminRequestHandler* RequestHandlerFactory::createRoomAdminRequestHandler(LoggedUser u, Room& r)
{
	RoomAdminRequestHandler* p = new RoomAdminRequestHandler(u, this->m_roomManager, *this, r);
	return p;
}

RoomMemberRequestHandler* RequestHandlerFactory::createRoomMemberRequestHandler(LoggedUser u, Room& r)
{
	RoomMemberRequestHandler* p = new RoomMemberRequestHandler(u, this->m_roomManager, *this, r);
	return p;
}

MenuRequestHandler* RequestHandlerFactory::createMenuRequestHandler(LoggedUser u)
{
	MenuRequestHandler* m = new MenuRequestHandler(u, this->m_roomManager, *this,this->m_StatisticsManager);
	return m;
}

StatisticsManager& RequestHandlerFactory::getStatisticsManager()
{
	return this->m_StatisticsManager;
}

RoomManager& RequestHandlerFactory::getRoomManager()
{
	return this->m_roomManager;
}

GameRequestHandler* RequestHandlerFactory::createGameRequestHandler(LoggedUser u)
{
	LoggedUser admin("");
	for (auto rd : this->m_roomManager.getRooms())
	{
		Room room = this->m_roomManager.getRoom(rd.id);
		for (auto player : room.getAllUsers())
		{
			if (player == u.getUsername())
			{
				admin = room.getAllUsers()[0];
			}
		}
	}
	GameRequestHandler* g = new GameRequestHandler(u, this->m_gameManager.getGame(admin), this->m_gameManager, *this);
	return g;
}

GameManager& RequestHandlerFactory::getGameManager()
{
	return this->m_gameManager;
}
