#pragma once
#include "IDatabase.h"
#include "callbackfunctions.h"
class SqliteDataBase : public IDatabase
{
public:
	SqliteDataBase() : IDatabase()
	{
		this->open();
	}
	virtual ~SqliteDataBase() = default;
	virtual void clear() override;
	virtual void open() override;
	virtual void close() override;
	virtual void add_user(UserDB) override;
	virtual bool does_user_exist(UserDB) override;
	virtual bool is_password_correct(UserDB) override;
	virtual std::list<Question> getQuestions(int) override;
	virtual float getPlayerAverageAnswerTime(std::string username) override;
	float getPlayerTotalAnswerTime(std::string username);
	virtual int getNumOfCorrectAnswers(std::string username) override;
	virtual int getNumOfTotalAnswers(std::string username) override;
	virtual int getNumOfPlayerGames(std::string username) override;
	virtual int getPlayerScore(std::string username) override;
	virtual std::vector<std::string> getHighScores() override;
	virtual void insertStatistics(PlayerResults) override;
	virtual void insertKey(std::string username, std::string key) override;
	virtual std::string getKey(std::string username) override;
private:
	sqlite3* db;

};