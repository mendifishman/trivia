#pragma once
#include "IDatabase.h"
#include <vector>
#include <string>
#include <set>
class LoggedUser
{
public:
    LoggedUser(std::string username)
    {
        this->m_username = username;
    }

    std::string getUsername() const
    {
        return this->m_username;
    }

    bool operator<(const LoggedUser& other) const
    {
        return m_username < other.m_username;
    }

    bool operator==(const LoggedUser& other) const
    {
        return m_username == other.m_username;
    }

private:
    std::string m_username;
};

class LoginManager
{
public:
	LoginManager(IDatabase* d) : m_database(d)
	{

	}
	bool signup(std::string username, std::string password, std::string email);
	bool login(std::string username, std::string password);
	bool logout(std::string username);
private:
	IDatabase* m_database;
	std::set<LoggedUser> m_loggedUsers;
};