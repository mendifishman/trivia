#include "MenuRequestHandler.h"
#include "RequestHandlerFactory.h"
#include "JsonRequestPacketDeserializer.h"
#include "JsonResponsePacketSerializer.h"



bool MenuRequestHandler::isRequestRelevant(RequestInfo r)
{
    return r.id <= (unsigned int)code::LOGOUT && r.id >= (unsigned int)code::CREATE_ROOM;
}

RequestResult MenuRequestHandler::handleRequest(RequestInfo r)
{
    code c = (code)r.id;
    RequestResult request_result;
    switch (c)
    {
        case code::CREATE_ROOM:
            request_result = this->createRoom(r);
            break;
        case code::GET_PLAYERS:
            request_result = this->getPlayersInRoom(r);
            break;
        case code::GET_ROOMS:
            request_result = this->getRooms(r);
            break;
        case code::JOIN_ROOM:
            request_result = this->joinRoom(r);
            break;
        case code::GET_STATS:
            request_result = this->getPersonalStats(r);
            break;
        case code::GET_HIGH_SCORES:
            request_result = this->getHighScore(r);
            break;
        case code::LOGOUT:
            request_result = this->signout(r);
            break;
        default:
            break;
    }
    return request_result;
}

RequestResult MenuRequestHandler::signout(RequestInfo)
{
    this->m_handlerFactory.getLoginManager().logout(this->m_user.getUsername());
    for (auto var : this->m_roomManager.getRooms())
    {
        if (this->m_user.getUsername() == m_roomManager.getRoom(var.id).getAllUsers()[0])
        {
            this->m_roomManager.deleteRoom(var.id);
        }
    }
    return  RequestResult{ JsonResponsePacketSerializer::serializeResponse(LogoutResponse{(unsigned int)code::OK}), this->m_handlerFactory.createLoginRequestHandler() };
}

RequestResult MenuRequestHandler::getRooms(RequestInfo r)
{
    return  RequestResult{ JsonResponsePacketSerializer::serializeResponse(GetRoomsResponse{(unsigned int)code::OK, this->m_roomManager.getRooms()}), this->m_handlerFactory.createMenuRequestHandler(this->m_user)};
}

RequestResult MenuRequestHandler::getPlayersInRoom(RequestInfo r)
{
    unsigned int id = JsonRequestPacketDeserializer::deserializeGetPlayersRequest(r.b).roomId;
    return  RequestResult{ JsonResponsePacketSerializer::serializeResponse(GetPlayersInRoomResponse{this->m_roomManager.getRoom(id).getAllUsers()}), this->m_handlerFactory.createMenuRequestHandler(this->m_user)};
}

RequestResult MenuRequestHandler::getPersonalStats(RequestInfo r)
{
    return RequestResult{ JsonResponsePacketSerializer::serializeResponse(getPersonalStatsResponse{(unsigned int)code::OK,this->m_handlerFactory.getStatisticsManager().getUserStatistics(this->m_user.getUsername())}), this->m_handlerFactory.createMenuRequestHandler(this->m_user) };
}

RequestResult MenuRequestHandler::getHighScore(RequestInfo r)
{
    return  RequestResult{ JsonResponsePacketSerializer::serializeResponse(getHighScoreResponse{(unsigned int)code::OK,this->m_handlerFactory.getStatisticsManager().getHighScore()}), this->m_handlerFactory.createMenuRequestHandler(this->m_user)};
}

RequestResult MenuRequestHandler::joinRoom(RequestInfo r)
{
    unsigned int id = JsonRequestPacketDeserializer::deserializeGetPlayersRequest(r.b).roomId;
    if (this->m_roomManager.getRoom(id).getAllUsers().size() < this->m_roomManager.getRoom(id).get_data().maxPlayers)
    {
        this->m_roomManager.getRoom(id).addUser(this->m_user);
        return  RequestResult{ JsonResponsePacketSerializer::serializeResponse(JoinRoomResponse{(unsigned int)code::OK}), this->m_handlerFactory.createRoomMemberRequestHandler(this->m_user,this->m_roomManager.getRoom(id)) };
    }
    return RequestResult{ JsonResponsePacketSerializer::serializeResponse(ErrorResponse{ "cant join, max players in room" }),this->m_handlerFactory.createMenuRequestHandler(this->m_user) };
}

RequestResult MenuRequestHandler::createRoom(RequestInfo r)
{
    RoomData room_data;
    CreateRoomRequest create_room = JsonRequestPacketDeserializer::deserializeCreateRoomRequest(r.b);
    room_data.id = this->m_roomManager.getRoomID();
    room_data.isActive = false;
    room_data.maxPlayers = create_room.maxUsers;
    room_data.name = create_room.roomName;
    room_data.numOfQuestionsInGame = create_room.questionCount;
    room_data.timePerQuestion = create_room.answerTimeout;
    room_data.is_deleted = false;
    this->m_roomManager.createRoom(this->m_user, room_data);
    return  RequestResult{ JsonResponsePacketSerializer::serializeResponse(CreateRoomResponse{(unsigned int)code::OK}), this->m_handlerFactory.createRoomAdminRequestHandler(this->m_user,this->m_roomManager.getRoom(room_data.id)) };
}
