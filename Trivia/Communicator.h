#pragma once
#include <WinSock2.h>
#include <Windows.h>
#include <queue>
#include <mutex>
#include <string>
#include <condition_variable>
#include <fstream>
#include <vector>
#include <algorithm>
#include <set>
#include "Helper.h"
#include <map>
#include "IRequestHandler.h"
#include "LoginRequestHandler.h"
#include <exception>
#include "Helper.h"
#include <set>
#include "Codes.h"
#include "RequestHandlerFactory.h"
#define PORT 5030

class Communicator
{
public:

	Communicator(RequestHandlerFactory& r);
	void set_socket(SOCKET s);
	void startHandleRequests();
	void bindAndListen();
	void handleNewClient(SOCKET);
private:
	SOCKET m_serverSocket;
	std::map<SOCKET, IRequestHandler*>	m_clients;
	RequestHandlerFactory& m_handlerFactory;

}; 