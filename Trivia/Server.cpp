#include "Server.h"
#include <exception>
#include <iostream>
#include <string>
#include <thread>
#include "SqliteDataBase.h"

	
Server::Server(): m_database(Helper::get_database()), m_handlerFactory(this->m_database), m_com(this->m_handlerFactory)
{
}



Server::~Server()
{
}

void Server::run()
{
	// this server use TCP. that why SOCK_STREAM & IPPROTO_TCP
	// if the server use UDP we will use: SOCK_DGRAM & IPPROTO_UDP
	_serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
	
	std::thread newthread(&Server::get_input, this);
	newthread.detach();
	this->m_com.set_socket(this->_serverSocket);
	this->m_com.bindAndListen();
	this->m_com.startHandleRequests();
}

void Server::get_input()
{
	std::string input = "";
	while (true)
	{
		std::cout << ">>> ";
		std::cin >> input;
		if (input == EXIT)
		{
			this->m_database->close();
			exit(0);
		}
	}
}
