#include "RoomManager.h"
roomID RoomManager::_id = 1;
bool RoomManager::createRoom(LoggedUser u, RoomData d)
{
    Room newroom;
    newroom.addUser(u);
    newroom.set_data(d);
    this->m_rooms.insert(std::pair<const roomID, Room>(d.id, newroom));
    _id++;
    return true;
}
bool RoomManager::deleteRoom(roomID ID)
{
    auto it = m_rooms.find(ID);
    if (it != m_rooms.end()) {
        m_rooms.erase(it);
        return true;
    }
    return false;
}

unsigned int RoomManager::getRoomState(roomID ID)
{
    auto it = m_rooms.find(ID);
    if (it != m_rooms.end()) {
        // Assuming the isActive field in RoomData represents the room state
        return it->second.get_data().isActive;

    }
    // Return a default state (0 or any other appropriate value) if the room doesn't exist
    return 0;
}

std::vector<RoomData> RoomManager::getRooms()
{
    std::vector<RoomData> room_list;
    for (auto& pair : m_rooms) {
        room_list.push_back(pair.second.get_data());
    }
    return room_list;
}

Room& RoomManager::getRoom(roomID ID)
{
    // Assuming that the room with the specified ID always exists
    return m_rooms.at(ID);
}