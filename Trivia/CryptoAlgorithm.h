#pragma once
#include <string>
#include "IDatabase.h"
#define MAX 10000

class CryptoAlgorithm
{
public:
	virtual std::string encrypt(const std::string& message) = 0;
	virtual std::string decrypt(const std::string& cypher) = 0;
};

class OTPCryptoAlgorithm : public CryptoAlgorithm
{
public:
	OTPCryptoAlgorithm(IDatabase* d);
	std::string encrypt(const std::string& message, const std::string& username);
	std::string decrypt(const std::string& cypher, const std::string& username);
	void generateKey(const std::string& username);

private:
	IDatabase* m_database;
	std::string key;
};