#include "LoginManager.h"
#include <algorithm>

bool LoginManager::signup(std::string username, std::string password, std::string email)
{
	UserDB u;
	u.email = email;
	u.password = password;
	u.username = username;
	this->m_database->add_user(u);
	return true;
}

bool LoginManager::login(std::string username, std::string password)
{
	UserDB u;
	u.password = password;
	u.username = username;
	if (this->m_database->does_user_exist(u) && this->m_database->is_password_correct(u))
	{
		LoggedUser l(u.username);
		this->m_loggedUsers.insert(l);
		return true;
	}
	return false;
}

bool LoginManager::logout(std::string username)
{
	UserDB u;
	u.username = username;
	LoggedUser l(u.username);
	if (this->m_database->does_user_exist(u) && (this->m_loggedUsers.find(l)!=this->m_loggedUsers.end()))
	{
		this->m_loggedUsers.erase(l);
		return true;
	}
	return false;
}
