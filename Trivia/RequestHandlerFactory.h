#pragma once
#include "LoginRequestHandler.h"
#include "MenuRequestHandler.h"
#include "RoomAdminRequestHandler.h"
#include "RoomMemberRequestHandler.h"
#include "LoginManager.h"
#include "RoomManager.h"
#include "StatisticsManager.h"
#include "IDatabase.h"
#include "GameManager.h"
#include "GameRequestHandler.h"
class RequestHandlerFactory
{
public:
	RequestHandlerFactory(IDatabase* d) : m_database(d) , m_loginManager(d), m_StatisticsManager(d), m_gameManager(d)
	{
		
	}
	LoginRequestHandler* createLoginRequestHandler();
	LoginManager& getLoginManager();
	RoomAdminRequestHandler* createRoomAdminRequestHandler(LoggedUser, Room&);
	RoomMemberRequestHandler* createRoomMemberRequestHandler(LoggedUser, Room&);
	MenuRequestHandler* createMenuRequestHandler(LoggedUser);
	StatisticsManager& getStatisticsManager();
	RoomManager& getRoomManager();
	GameRequestHandler* createGameRequestHandler(LoggedUser);
	GameManager& getGameManager();

private:
	LoginManager m_loginManager;
	IDatabase* m_database;
	RoomManager m_roomManager;
	GameManager m_gameManager;
	StatisticsManager m_StatisticsManager;

};