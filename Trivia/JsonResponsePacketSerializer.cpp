#include "JsonResponsePacketSerializer.h"
#include "Codes.h"

Buffer JsonResponsePacketSerializer::serializeResponse(LoginResponse answer)
{
    json _json;
    _json["status"] = answer._status;
    return encodeMsg(answer._status, _json);
}

Buffer JsonResponsePacketSerializer::serializeResponse(SignupResponse answer)
{
    json _json;
    _json["status"] = answer._status;
    return encodeMsg(answer._status, _json);
}

Buffer JsonResponsePacketSerializer::serializeResponse(ErrorResponse answer)
{
    json _json;
    _json["message"] = answer._errMsg;
    return encodeMsg(static_cast<unsigned char>(code::NOT_OK), _json);
}

json JsonResponsePacketSerializer::getJson()
{
    json _json;
    return _json;
}

Buffer JsonResponsePacketSerializer::encodeMsg(unsigned char code, json _json)
{
    Buffer b;
    b.push_back(code);
    int number = static_cast<int>(_json.size());
    char bytes[sizeof(int)];
    for (int i = sizeof(int) - 1; i >= 0; --i) {
        bytes[i] = static_cast<char>(number & 0xFF);
        number >>= 8;
    }
    for (int i = 0; i < sizeof(int); i++)
    {
        b.push_back(bytes[i]);
    }
    std::string jsonString = _json.dump();

    // Push each character of the string into the buffer
    for (const char& character : jsonString) {
        b.push_back(static_cast<unsigned char>(character));
    }
    return b;
}

Buffer JsonResponsePacketSerializer::serializeResponse(LogoutResponse answer)
{
    json _json;
    _json["status"] = answer._status;
    return encodeMsg(answer._status, _json);
}

Buffer JsonResponsePacketSerializer::serializeResponse(GetRoomsResponse answer)
{
    json _json;
    _json["status"] = answer._status;
    json roomsArray = json::array();
    for (const auto& room : answer._rooms)
    {
        json roomObject;
        roomObject["id"] = room.id;
        roomObject["name"] = room.name;
        roomObject["maxPlayers"] = room.maxPlayers;
        roomObject["numOfQuestionsInGame"] = room.numOfQuestionsInGame;
        roomObject["timePerQuestion"] = room.timePerQuestion;
        roomObject["isActive"] = room.isActive;

        roomsArray.push_back(roomObject);
    }
    _json["rooms"] = roomsArray;
    return encodeMsg(answer._status, _json);
}

Buffer JsonResponsePacketSerializer::serializeResponse(GetPlayersInRoomResponse answer)
{
    json _json;
    _json["players"] = answer._playres;
    return encodeMsg(static_cast<unsigned char>(code::OK), _json);
}

Buffer JsonResponsePacketSerializer::serializeResponse(JoinRoomResponse answer)
{
    json _json;
    _json["status"] = answer._status;
    return encodeMsg(answer._status, _json);
}

Buffer JsonResponsePacketSerializer::serializeResponse(CreateRoomResponse answer)
{
    json _json;
    _json["status"] = answer._status;
    return encodeMsg(answer._status, _json);
}

Buffer JsonResponsePacketSerializer::serializeResponse(getHighScoreResponse answer)
{
    json _json;
    _json["status"] = answer._status;
    _json["statistics"] = answer._statistics;
    return encodeMsg(answer._status, _json);
}

Buffer JsonResponsePacketSerializer::serializeResponse(getPersonalStatsResponse answer)
{
    json _json;
    _json["status"] = answer._status;
    _json["statistics"] = answer._statistics;
    return encodeMsg(answer._status, _json);
}

Buffer JsonResponsePacketSerializer::serializeResponse(CloseRoomResponse answer)
{
    json _json;
    _json["status"] = answer._status;
    return encodeMsg(answer._status, _json);
}

Buffer JsonResponsePacketSerializer::serializeResponse(StartGameResponse answer)
{
    json _json;
    _json["status"] = answer._status;
    return encodeMsg(answer._status, _json);
}

Buffer JsonResponsePacketSerializer::serializeResponse(GetRoomStateResponse answer)
{
    json _json;
    _json["status"] = answer._status;
    _json["hasGameBegun"] = answer._hasGameBegun;
    _json["players"] = answer._players;
    _json["questionCount"] = answer._questionCount;
    _json["answerTimeout"] = answer._answerTimeout;
    return encodeMsg(answer._status, _json);
}

Buffer JsonResponsePacketSerializer::serializeResponse(LeaveRoomResponse answer)
{
    json _json;
    _json["status"] = answer._status;
    return encodeMsg(answer._status, _json);
}

Buffer JsonResponsePacketSerializer::serializeResponse(GetGameResultsResponse answer)
{
    json _json;
    _json["status"] = answer._status;
    json resultsArray = json::array();
    for (const auto& result : answer.results)
    {
        json resultObject;
        resultObject["username"] = result.username;
        resultObject["correctAnswerCount"] = result.correctAnswerCount;
        resultObject["wrongAnswerCount"] = result.wrongAnswerCount;
        resultObject["averageAnswerTime"] = result.averageAnswerTime;

        resultsArray.push_back(resultObject);
    }
    _json["results"] = resultsArray;
    return encodeMsg(answer._status, _json);
}

Buffer JsonResponsePacketSerializer::serializeResponse(SubmitAnswerResponse answer)
{
    json _json;
    _json["status"] = answer._status;
    _json["correctAnswerId"] = answer.correctAnswerId;
    return encodeMsg(answer._status, _json);
}

Buffer JsonResponsePacketSerializer::serializeResponse(GetQuestionResponse answer)
{
    json _json;
    _json["status"] = answer._status;
    _json["question"] = answer.question;
    json answersObject = json::object();
    for (const auto& entry : answer.answers)
    {
        answersObject[std::to_string(entry.first)] = entry.second;
    }
    _json["answers"] = answersObject;
    return encodeMsg(answer._status, _json);
}

Buffer JsonResponsePacketSerializer::serializeResponse(LeaveGameResponse answer)
{
    json _json;
    _json["status"] = answer._status;
    return encodeMsg(answer._status, _json);
}
