#pragma once

#include "IRequestHandler.h"
#include "LoginManager.h"
#include "RoomManager.h"
#include "Codes.h"

class RequestHandlerFactory;
class RoomAdminRequestHandler : public IRequestHandler
{
public:
	RoomAdminRequestHandler(LoggedUser u, RoomManager& rm, RequestHandlerFactory& h, Room& r) : IRequestHandler(), m_user(u), m_roomManager(rm), m_handlerFactory(h), m_room(r)
	{
	}
	virtual bool isRequestRelevant(RequestInfo r) override;
	virtual RequestResult handleRequest(RequestInfo r) override;
	RequestResult closeRoom(RequestInfo);
	RequestResult startGame(RequestInfo);
	RequestResult getRoomState(RequestInfo);

private:
	LoggedUser m_user;
	RoomManager& m_roomManager;
	RequestHandlerFactory& m_handlerFactory;
	Room& m_room;
};