#include "callbackfunctions.h"
#include <string>
#include <list>
#include "IDatabase.h"

int callback_does_exist(void* data, int argc, char** argv, char** azColName)
{
	bool* is = (bool*)data;
	if (argc < 1)
	{
		*is = false;
	}
	else
	{
		*is = true;
	}
	return 0;
}

int does_nothing(void* data, int argc, char** argv, char** azColName)
{
    return 0;
}

int callback_get_password(void* data, int argc, char** argv, char** azColName)
{
	std::string* password = (std::string*)data;
	for (int i = 0; i < argc; i++) {
		if (std::string(azColName[i]) == "PASSWORD") {
			*password = argv[i];
		}
	}
	return 0;
}

int callback_get_question(void* data, int argc, char** argv, char** azColName)
{
	std::list<Question>* questions = (std::list<Question>*)data;
	Question q;
	for (int i = 0; i < argc; i++) {
		if (std::string(azColName[i]) == "QUESTION_TEXT") {
			q.questoin_text = argv[i];
		}
		else if (std::string(azColName[i]) == "ANSWER1" || std::string(azColName[i]) == "ANSWER2" || std::string(azColName[i]) == "ANSWER3" || std::string(azColName[i]) == "ANSWER4") {
			q.answers.push_back(argv[i]);
		}
		else if (std::string(azColName[i]) == "CORRECT_ANSWER") {
			q.corrent_answer = atoi(argv[i]);
		}
	}
	questions->push_back(q);
	return 0;
}

int callback_get_time(void* data, int argc, char** argv, char** azColName)
{
	float* time = (float*)data;
	for (int i = 0; i < argc; i++) {
		if (std::string(azColName[i]) == "TIME") {
			*time = std::stof(argv[i]);
		}
	}
	return 0;
}

int callback_get_num(void* data, int argc, char** argv, char** azColName)
{
	int* time = (int*)data;
	for (int i = 0; i < argc; i++) {
		if (std::string(azColName[i]) == "CORRECT_ANSWERS" || std::string(azColName[i]) == "TOTAL_ANSWERS" || std::string(azColName[i]) == "NUM_OF_GAMES") {
			*time = atoi(argv[i]);
		}
	}
	return 0;
}

int callback_get_high_scores(void* data, int argc, char** argv, char** azColName)
{
	std::vector<std::string>* scores = (std::vector<std::string>*)data;
	std::string str = "Name: ";
	for (int i = 0; i < argc; i++) {
		if (std::string(azColName[i]) == "USERNAME" ) {
			str += argv[i];
			str += ", Score: ";
		}
		else if (std::string(azColName[i]) == "SCORE") {
			str += argv[i];
		}
	}
	scores->push_back(str);
	return 0;
}
