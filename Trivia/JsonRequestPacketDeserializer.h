#pragma once
#include "Requests.h"
#include <nlohmann/json.hpp>
#include <vector>
typedef std::vector<unsigned char> Buffer;
using json = nlohmann::json;
class JsonRequestPacketDeserializer
{
public:
	static LoginRequest deserializeLoginRequest(Buffer b);
	static SignupRequest deserializeSignupRequest(Buffer b);
	static GetPlayersInRoomRequest deserializeGetPlayersRequest(Buffer b);
	static JoinRoomRequest deserializeJoinRoomRequest(Buffer b);
	static CreateRoomRequest deserializeCreateRoomRequest(Buffer b);
	static SubmitAnswerRequest deserializeSubmitAnswerRequest(Buffer b);
};