#include "Helper.h"


using std::string;



// recieves the type code of the message from socket (3 bytes)
// and returns the code. if no message found in the socket returns 0 (which means the client disconnected)
int Helper::getMessageTypeCode(const SOCKET sc)
{
	char* data = new char[1];
	int res = recv(sc, data, 1, 0);
	if (res == INVALID_SOCKET)
	{
		std::string s = "Error while recieving from socket: ";
		s += std::to_string(sc);
		throw std::exception(s.c_str());
	}
	int res2 = (int)data[0];
	delete[] data;
	return  res2;
}



// recieve data from socket according byteSize
// returns the data as int
int Helper::getIntPartFromSocket(const SOCKET sc, const int bytesNum)
{
	return atoi(getPartFromSocket(sc, bytesNum, 0).c_str());
}

// recieve data from socket according byteSize
// returns the data as string
string Helper::getStringPartFromSocket(const SOCKET sc, const int bytesNum)
{
	return getPartFromSocket(sc, bytesNum, 0);
}

// return string after padding zeros if necessary
string Helper::getPaddedNumber(const int num, const int digits)
{
	std::ostringstream ostr;
	ostr << std::setw(digits) << std::setfill('0') << num;
	return ostr.str();

}

std::string Helper::vectorToString(const Buffer& vec)
{
	std::string str(vec.begin(), vec.end());
	return str;
}

Buffer Helper::getBufferFromSocket(const SOCKET sc)
{
    char* data = new char[4];
    int res = recv(sc, data, 4, 0);
    if (res == INVALID_SOCKET)
    {
        std::string s = "Error while receiving from socket: ";
        s += std::to_string(sc);
        throw std::exception(s.c_str());
    }

	int size = ntohl(*(int*)data); // Convert size to host byte order

    delete[] data;

    char* data2 = new char[size];
    res = recv(sc, data2, size, 0);
    if (res == INVALID_SOCKET)
    {
        std::string s = "Error while receiving from socket: ";
        s += std::to_string(sc);
        throw std::exception(s.c_str());
    }

    Buffer b;
    for (int i = 0; i < size; i++)
    {
        b.push_back(data2[i]);
    }

    delete[] data2;
    return b;
}

std::vector<unsigned char> Helper::stringToVector(const std::string& str)
{
	std::vector<unsigned char> vec;
	for (auto it = str.begin(); it != str.end(); ++it) {
		vec.push_back(static_cast<unsigned char>(*it));
	}
	return vec;
}

json Helper::extract_json(const Buffer& msg)
{
	// Check if the first byte is '1' to ensure that the data in the vector is a JSON object
	/*
	if (msg.size() < 2 || msg[0] != '1')
	{
		cout << "the data isnt a json object" << endl;
	}
	*/
	// Extract the size of the JSON object from the second byte
	size_t json_size = msg[1];
	// Convert the bytes of the JSON object back into a string
	string json_str(msg.begin() + 2, msg.begin() + 2 + json_size);
	// Parse the string into a nlohmann::json object
	json json_obj = json::parse(json_str);
	return json_obj;
}

RequestInfo Helper::getRequestFromSocket(Buffer b)
{
	RequestInfo request;
	request.id = (unsigned int)b[0];
	unsigned char num[4];
	num[0] = b[1];
	num[1] = b[2];
	num[2] = b[3];
	num[3] = b[4];
	int size = *(int*)num;

	return request;

}

void Helper::sendBufferToClient(Buffer b, SOCKET client_socket)
{
	unsigned char* data = new unsigned char[b.size()];
	for (int i = 0; i < b.size(); i++)
	{
		data[i] = b[i];
	}
	
	if (send(client_socket, (const char *)data, b.size(), 0) == INVALID_SOCKET)
	{
		throw std::exception("Error while sending message to client");
	}
}

SqliteDataBase* Helper::get_database()
{
	SqliteDataBase* s = new SqliteDataBase();
	return s;
}

// recieve data from socket according byteSize
// this is private function
std::string Helper::getPartFromSocket(const SOCKET sc, const int bytesNum)
{
	return getPartFromSocket(sc, bytesNum, 0);
}

// send data to socket
// this is private function
void Helper::sendData(const SOCKET sc, const std::string message)
{
	const char* data = message.c_str();

	if (send(sc, data, message.size(), 0) == INVALID_SOCKET)
	{
		throw std::exception("Error while sending message to client");
	}
}

std::string Helper::getPartFromSocket(const SOCKET sc, const int bytesNum, const int flags)
{
	if (bytesNum == 0)
	{
		return "";
	}

	char* data = new char[bytesNum + 1];
	int res = recv(sc, data, bytesNum, flags);
	if (res == INVALID_SOCKET)
	{
		std::string s = "Error while recieving from socket: ";
		s += std::to_string(sc);
		throw std::exception(s.c_str());
	}
	data[bytesNum] = 0;
	std::string received(data);
	delete[] data;
	return received;
}
