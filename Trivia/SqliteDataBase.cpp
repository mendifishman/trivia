#include "SqliteDataBase.h"
#include <iostream>

void SqliteDataBase::clear()
{
	const char* sqlStatement = "DELETE FROM USERS WHERE 1=1;";
	char* errMessage = nullptr;
	int res = sqlite3_exec(db, sqlStatement, nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK)
	{
		this->db = nullptr;
		std::cout << "Failed to clear DB" << std::endl;
	}
}

void SqliteDataBase::open()
{
	std::string dbFileName = "triviaDB.sqlite";
	int file_exist = 1;
	int res = sqlite3_open(dbFileName.c_str(), &this->db);
	//*file_exist = _access(dbFileName.c_str(), 0);*/the function didnt work for some reason...
	if (res != SQLITE_OK)
	{
		this->db = nullptr;
		std::cout << "Failed to open DB" << std::endl;
	}
	if (!file_exist)
	{
		const std::string create = "CREATE TABLE USERS (USERNAME TEXT UNIQUE NOT NULL,PASSWORD TEXT NOT NULL, EMAIL TEXT NOT NULL);";
		char* errMessage = nullptr;
		res = sqlite3_exec(db, create.c_str(), nullptr, nullptr, &errMessage);
		if (res != SQLITE_OK)
		{
			this->db = nullptr;
			std::cout << "Failed to open DB" << std::endl;
		}
		std::cout << "Created a new db\n";
	}
}

void SqliteDataBase::close()
{
	sqlite3_close(this->db);
	db = nullptr;
}

void SqliteDataBase::add_user(UserDB user)
{
	std::string str = "INSERT INTO USERS VALUES('" + user.username +"','" + user.password +  "','" + user.email + "');";
	char* errMessage = nullptr;
	int res = sqlite3_exec(db, str.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK) {
		std::cout << "An error happend when trying to get if album exists\n";
	}
}

bool SqliteDataBase::does_user_exist(UserDB user)
{
	bool is = false;
	std::string str = "SELECT * FROM USERS WHERE USERNAME = '" + user.username +  "';";
	char* errMessage = nullptr;
	int res = sqlite3_exec(db, str.c_str(), callback_does_exist, &is, &errMessage);
	if (res != SQLITE_OK) {
		std::cout << "An error happend when trying to get if album exists\n";
	}
	return is;
}

bool SqliteDataBase::is_password_correct(UserDB user)
{
	std::string password;
	std::string str = "SELECT PASSWORD FROM USERS WHERE USERNAME = '" + user.username + "';";
	char* errMessage = nullptr;
	int res = sqlite3_exec(db, str.c_str(),callback_get_password, &password, &errMessage);
	if (res != SQLITE_OK) {
		std::cout << "An error happend when trying to get password\n";
	}
	return password == user.password;
}

std::list<Question> SqliteDataBase::getQuestions(int num)
{
	std::list<Question> q;
	std::string str = "SELECT* FROM QUESTION ORDER BY RANDOM() LIMIT " + std::to_string(num) + ";";
	char* errMessage = nullptr;
	int res = sqlite3_exec(db, str.c_str(), callback_get_question, &q, &errMessage);
	if (res != SQLITE_OK) {
		std::cout << "An error happend when trying to get password\n";
	}
	return q;
}

float SqliteDataBase::getPlayerAverageAnswerTime(std::string username)
{
	return this->getPlayerTotalAnswerTime(username) / this->getNumOfTotalAnswers(username);
}

float SqliteDataBase::getPlayerTotalAnswerTime(std::string username)
{
	float time;
	std::string str = "SELECT TIME FROM STATISTICS WHERE USERNAME = '" + username + "' ;";
	char* errMessage = nullptr;
	int res = sqlite3_exec(db, str.c_str(), callback_get_time, &time, &errMessage);
	if (res != SQLITE_OK) {
		std::cout << "An error happend when trying to get password\n";
	}
	return time;
}

int SqliteDataBase::getNumOfCorrectAnswers(std::string username)
{
	int num;
	std::string str = "SELECT CORRECT_ANSWERS FROM STATISTICS WHERE USERNAME = '" + username + "' ;";
	char* errMessage = nullptr;
	int res = sqlite3_exec(db, str.c_str(), callback_get_num, &num, &errMessage);
	if (res != SQLITE_OK) {
		std::cout << "An error happend when trying to get password\n";
	}
	return num;
}

int SqliteDataBase::getNumOfTotalAnswers(std::string username)
{
	int num;
	std::string str = "SELECT TOTAL_ANSWERS FROM STATISTICS WHERE USERNAME = '" + username + "' ;";
	char* errMessage = nullptr;
	int res = sqlite3_exec(db, str.c_str(), callback_get_num, &num, &errMessage);
	if (res != SQLITE_OK) {
		std::cout << "An error happend when trying to get password\n";
	}
	return num;
}

int SqliteDataBase::getNumOfPlayerGames(std::string username)
{
	int num;
	std::string str = "SELECT NUM_OF_GAMES FROM STATISTICS WHERE USERNAME = '" + username + "' ;";
	char* errMessage = nullptr;
	int res = sqlite3_exec(db, str.c_str(), callback_get_num, &num, &errMessage);
	if (res != SQLITE_OK) {
		std::cout << "An error happend when trying to get password\n";
	}
	return num;
}

int SqliteDataBase::getPlayerScore(std::string username)
{
	return (int)(this->getNumOfCorrectAnswers(username) / this->getPlayerAverageAnswerTime(username) * 100);
}

std::vector<std::string> SqliteDataBase::getHighScores()
{
	std::vector<std::string> v;
	std::string str = "SELECT USERNAME, CAST(ROUND((CORRECT_ANSWERS / (TIME / TOTAL_ANSWERS)) * 100) AS INTEGER) AS SCORE FROM STATISTICS ORDER BY SCORE DESC LIMIT 3;";
	char* errMessage = nullptr;
	int res = sqlite3_exec(db, str.c_str(), callback_get_high_scores, &v, &errMessage);
	if (res != SQLITE_OK) {
		std::cout << "An error happend when trying to get password\n";
	}
	return v;
}

void SqliteDataBase::insertStatistics(PlayerResults p)
{
	std::string selectQuery = "SELECT * FROM STATISTICS WHERE USERNAME = '" + p.username + "';";
	bool isExist = false;
	char* errMsg = nullptr;

	int res = sqlite3_exec(db, selectQuery.c_str(), callback_does_exist, &isExist, &errMsg);
	if (res != SQLITE_OK) {
		std::cout << "An error occurred when trying to check if the username exists.\n";
		sqlite3_free(errMsg);
		return;
	}

	if (isExist)
	{
		std::string updateQuery = "UPDATE STATISTICS SET TIME = TIME + " + std::to_string(p.averageAnswerTime * (p.correctAnswerCount + p.wrongAnswerCount)) +
			", CORRECT_ANSWERS = CORRECT_ANSWERS + " + std::to_string(p.correctAnswerCount) +
			", TOTAL_ANSWERS = TOTAL_ANSWERS + " + std::to_string(p.correctAnswerCount + p.wrongAnswerCount) +
			", NUM_OF_GAMES = NUM_OF_GAMES + 1" +
			" WHERE USERNAME = '" + p.username + "';";

		res = sqlite3_exec(db, updateQuery.c_str(), nullptr, nullptr, &errMsg);
		if (res != SQLITE_OK) {
			std::cout << "An error occurred when trying to update statistics.\n";
			sqlite3_free(errMsg);
			return;
		}
	}
	else
	{
		std::string insertQuery = "INSERT INTO STATISTICS VALUES('" + p.username + "', " +
			std::to_string(p.averageAnswerTime * (p.correctAnswerCount + p.wrongAnswerCount)) + ", " +
			std::to_string(p.correctAnswerCount) + ", " +
			std::to_string(p.correctAnswerCount + p.wrongAnswerCount) + ", 1);";

		res = sqlite3_exec(db, insertQuery.c_str(), nullptr, nullptr, &errMsg);
		if (res != SQLITE_OK) {
			std::cout << "An error occurred when trying to insert statistics.\n";
			sqlite3_free(errMsg);
			return;
		}
	}
}

void SqliteDataBase::insertKey(std::string username, std::string key)
{
	std::string insertQuery = "INSERT INTO KEYS VALUES('" + username + "','" + key +  "');";
	char* errMsg = nullptr;

	int res = sqlite3_exec(db, insertQuery.c_str(), nullptr, nullptr, &errMsg);
	if (res != SQLITE_OK) {
		std::cout << "An error occurred when trying to check if the username exists.\n";
		sqlite3_free(errMsg);
		return;
	}
}

std::string SqliteDataBase::getKey(std::string username)
{
	std::string key;
	std::string str = "SELECT KEY FROM KEYS WHERE USERNAME = '" + username + "';";
	char* errMessage = nullptr;
	int res = sqlite3_exec(db, str.c_str(), callback_get_password, &key, &errMessage);
	if (res != SQLITE_OK) {
		std::cout << "An error happend when trying to get password\n";
	}
	return key;
}
