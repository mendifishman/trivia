#pragma once

#include "IRequestHandler.h"
#include "LoginManager.h"
#include "GameManager.h"
#include "RoomManager.h"
#include "Codes.h"

class RequestHandlerFactory;
class GameRequestHandler : public IRequestHandler
{
public:
	GameRequestHandler(LoggedUser u, Game& g, GameManager& gm, RequestHandlerFactory& rh) : IRequestHandler(), m_user(u), m_game(g), m_gameManager(gm), m_handlerFacroty(rh)
	{
	}
	virtual bool isRequestRelevant(RequestInfo r) override;
	virtual RequestResult handleRequest(RequestInfo r) override;
	RequestResult getQuestion(RequestInfo);
	RequestResult submitAnswer(RequestInfo);
	RequestResult getGameResults(RequestInfo);
	RequestResult leaveGame(RequestInfo);


private:
	Game& m_game;
	LoggedUser m_user;
	GameManager& m_gameManager;
	RequestHandlerFactory& m_handlerFacroty;
};