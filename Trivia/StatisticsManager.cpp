#include "StatisticsManager.h"

std::vector<std::string> StatisticsManager::getHighScore()
{
    return this->m_database->getHighScores();
}

std::vector<std::string> StatisticsManager::getUserStatistics(std::string username)
{
    std::vector<std::string> v;
    v.push_back(std::to_string(this->m_database->getNumOfCorrectAnswers(username)));
    v.push_back(std::to_string(this->m_database->getNumOfPlayerGames(username)));
    v.push_back(std::to_string(this->m_database->getNumOfTotalAnswers(username)));
    v.push_back(std::to_string(this->m_database->getPlayerAverageAnswerTime(username)));
    v.push_back(std::to_string(this->m_database->getPlayerScore(username)));
    return v;
}
