#include "Communicator.h"
#include "JsonResponsePacketSerializer.h"




Communicator::Communicator(RequestHandlerFactory& r): m_handlerFactory(r)
{
}

void Communicator::set_socket(SOCKET s)
{
	this->m_serverSocket = s;
}

void Communicator::startHandleRequests()
{
	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		std::cout << "Accepting clients..." << std::endl;
		// this accepts the client and create a specific socket from server to this client
	   // the process will not continue until a client connects to the server
		SOCKET client_socket = accept(this->m_serverSocket, NULL, NULL);
		if (client_socket == INVALID_SOCKET)
			throw std::exception(__FUNCTION__);
		LoginRequestHandler  new_handler(this->m_handlerFactory);
		this->m_clients.insert(std::pair< SOCKET, IRequestHandler*>(client_socket, &new_handler));
		std::thread newthread(&Communicator::handleNewClient, this, client_socket);
		newthread.detach();
		std::cout << "Client accepted. Server and client can speak" << std::endl;
		// the function that handle the conversation with the client
	}
}

void Communicator::bindAndListen()
{
	struct sockaddr_in sa = { 0 };
	std::cout << "Starting...\n";
	sa.sin_port = htons(PORT); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"
	std::cout << "Binding...\n";
	// Connects between the socket and the configuration (port and etc..)
	if (bind(this->m_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");
	std::cout << "Listening...\n";
	// Start listening for incoming requests of clients
	if (listen(this->m_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
}

void Communicator::handleNewClient(SOCKET client_socket)
{
	try
	{
		while (true)
		{
			RequestInfo request;
			request.id = Helper::getMessageTypeCode(client_socket);
			request.b = Helper::getBufferFromSocket(client_socket);
			request.receivalTime = std::chrono::system_clock::now();
			if (this->m_clients.at(client_socket)->isRequestRelevant(request))
			{
				RequestResult r = this->m_clients.at(client_socket)->handleRequest(request);
				auto it = this->m_clients.find(client_socket);
				it->second = r.new_handler;
				Helper::sendBufferToClient(r.b, client_socket);
			}
			else
			{
				Helper::sendBufferToClient(JsonResponsePacketSerializer::serializeResponse(ErrorResponse{ "request id doesnt match handler" }), client_socket);
			}

		}
	}
	catch (...)
		{
		}
}